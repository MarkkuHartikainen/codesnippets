﻿using System.Collections.Generic;
using UnityEngine;
using Firebase.Firestore;
using Firebase.Extensions;
using System;

// This script listens the changes in firebase documents

public class FirebaseListener : MonoBehaviour
{

    private void Start()
    {
        // Listen changes in documents under "users"
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        Query query = db.Collection("users");

        // When document under "users" is modified, go through all the changes
        ListenerRegistration listener = query.Listen(snapshot =>
        {
            // Different actions based on the document change type
            foreach (DocumentChange change in snapshot.GetChanges())
            {
                // If new document is added to users, print "new user"
                if (change.ChangeType == DocumentChange.Type.Added)
                {
                    Debug.Log(String.Format("New user: {0}", change.Document.Id));
                }
                // If document under users is modified, check user log in status
                else if (change.ChangeType == DocumentChange.Type.Modified)
                {
                    // Check if user is logged in or out
                    CheckLogStatus("users", change.Document.Id.ToString());
                    Debug.Log(String.Format("Modified user: {0}", change.Document.Id));

                }
                // If document under users is removed, print "removed user"
                else if (change.ChangeType == DocumentChange.Type.Removed)
                {
                    Debug.Log(String.Format("Removed user: {0}", change.Document.Id));
                }
            }               
        });
    }

    // Check if user is logged in or out, from specific document
    private void CheckLogStatus(string collection, string documentName)
    {
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        DocumentReference docRef = db.Collection(collection).Document(documentName);

        docRef.GetSnapshotAsync().ContinueWithOnMainThread(task =>
        {
            // When checking user log in -status, game needs information about user id, name, color and whether the user is currently present
            int id = 0;
            string name = "";
            Color color = StarManager.defaultStarColor;
            bool present = false;

            DocumentSnapshot snapshot = task.Result;
            Dictionary<string, object> dictionary = snapshot.ToDictionary();

            // Go through user document and find the needed information.
            // Information is saved in "key" and "value" pairs.
            // Key names in my firebase are: "ID", "First", "Color" and "Present".

            // Check each "key" and "value" pair from user document
            foreach (KeyValuePair<string, object> pair in dictionary)
            {
                // Save user ID value from document.
                if (pair.Key == "ID")
                {
                    id = int.Parse(pair.Value.ToString());
                }

                // Save user first name value from document
                if (pair.Key == "First")
                {
                    name = pair.Value.ToString();
                    continue;
                }

                // Save user color value from document
                if (pair.Key == "Color")
                {
                    color = GameManager.starManager.GetComponent<StarManager>().RGBConverter(pair.Value.ToString());
                }

                // Save user present value from document
                if (pair.Key.ToString() == "Present")
                {
                    if (pair.Value.ToString().Trim().ToLower() == "true")
                    {
                        present = true;
                    }
                    continue;
                }
            }
                    
            // If user is present spawn star. Star will have correct user ID, name and color values
            if (present)
            {
                GameManager.starManager.GetComponent<StarManager>().SpawnStar(id, name, color);
            }
            // If user is not present, destroy star with that user ID.
            else
            {
                GameManager.starManager.GetComponent<StarManager>().DestroyStar(id);
            }
        });
    }

    // Read certain value from any firebase document. Function needs to know correct document collection, document name and the key we are looking for.
    private string CheckKeyValue(string collection, string documentName, string key)
    {
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        DocumentReference docRef = db.Collection(collection).Document(documentName);
        string value = "";

        docRef.GetSnapshotAsync().ContinueWithOnMainThread(task =>
        {
            DocumentSnapshot snapshot = task.Result;
            Dictionary<string, object> dictionary = snapshot.ToDictionary();

            foreach (KeyValuePair<string, object> pair in dictionary)
            {
                if (pair.Key == key)
                {
                    value = pair.Value.ToString();
                }
            }
        });
        return value;
    }

    // These are example functions for adding users in the firebase. These are not used in this game.
    void AddFirstUser()
    {
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        DocumentReference docRef = db.Collection("users").Document("duck");
        Dictionary<string, object> user = new Dictionary<string, object>
        {
            { "First", "Donald" },
            { "Last", "Duck" },
            { "Born", 1934 },
        };
        docRef.SetAsync(user).ContinueWithOnMainThread(task => {
            Debug.Log("Added data to the alovelace document in the users collection.");
        });
    }

    void AddSecondUser()
    {
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        DocumentReference docRef = db.Collection("users").Document("pooh");
        Dictionary<string, object> user = new Dictionary<string, object>
        {
            { "First", "Winnie" },
            { "Middle", "The" },
            { "Last", "Pooh" },
            { "Born", 1921 }
        };
        docRef.SetAsync(user).ContinueWithOnMainThread(task => {
            Debug.Log("Added data to the aturing document in the users collection.");
        });
    }
}
