﻿using System.Collections;
using Firebase.Auth;
using UnityEngine;

// This script handles the "user profile log in" aka. "email and password login" to the firebase
// You can create users directly in the firebase console

public class FirebaseEmailLogIn : MonoBehaviour
{
    // Login canvas of the game
    public GameObject loginCanvas;

    // Login coroutine
    private Coroutine _loginCoroutine = null;

    // Login to firebase with email and password, function is called from login -button
    public void LogIn(string email, string password)
    {
        // If login coroutine is not already running, run login coroutine
        if (_loginCoroutine == null)
        {
            _loginCoroutine = StartCoroutine(LoginCoroutine(email, password));
        }
    }

    // The login coroutine
    private IEnumerator LoginCoroutine(string email, string password)
    {       
        // Cool firebase functions for login
        var auth = FirebaseAuth.DefaultInstance;
        var loginTask = auth.SignInWithEmailAndPasswordAsync(email, password);

        // Wait until login is completed
        yield return new WaitUntil(() => loginTask.IsCompleted);

        // If login failed in one way or another, show "login failed canvas" and return
        if (loginTask.IsCanceled)
        {
            Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
            loginCanvas.GetComponent<MainMenuCanvas>().LoginFailed();
            _loginCoroutine = null;
            yield break;
        }
        if (loginTask.IsFaulted)
        {
            Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + loginTask.Exception);
            loginCanvas.GetComponent<MainMenuCanvas>().LoginFailed();
            _loginCoroutine = null;
            yield break;
        }

        // If login succeed, save newUser and show "login success canvas"
        FirebaseUser newUser = loginTask.Result;
        Debug.LogFormat("User signed in successfully: {0} ({1})",
            newUser.DisplayName, newUser.UserId);

        loginCanvas.GetComponent<MainMenuCanvas>().LoginSuccess();
        _loginCoroutine = null;
    }

}
