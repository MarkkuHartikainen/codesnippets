﻿using UnityEngine;

// This script makes gameObject to "orbit" or rotate around certain transform

public class OrbitAroundCenter : MonoBehaviour
{
    // Center point of rotation
    public Transform center;

    // Around what axis rotation is happening
    private Vector3 axis = Vector3.up;

    // Current target position
    private Vector3 desiredPosition;

    // Rotation radius and speed values
    public float radius = 2.0f;
    public float radiusSpeed = 0.5f;
    public float rotationSpeed = 80.0f;

    // Set start position and radius
    void Start()
    {
        transform.position = (transform.position - center.position).normalized * radius + center.position;
        radius = 2.0f;
    }

    // Handles the actual rotation
    void Update()
    {
        transform.RotateAround(center.position, axis, rotationSpeed * Time.deltaTime);
        desiredPosition = (transform.position - center.position).normalized * radius + center.position;
        transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Time.deltaTime * radiusSpeed);
    }
}
   