﻿using System.Collections;
using UnityEngine;

// Moves gameObject smoothly vertically aka. up and down

public class UpAndDown : MonoBehaviour
{
    // Movement speed and vertical movement amount
    public float speed = 2f;
    public float amount = 5f;
   
    // Timer used for the motion
    private float timer = 0f;

    // Current position information
    private Vector3 pos = new Vector3(0f, 0f, 0f);

    // Movement happens only when move is true
    private bool move = true;

    // Converts movement speed and amount values to correct scale and starts the movement coroutine
    void Start()
    {
        speed = speed / 100;
        amount = amount / 100;
        StartCoroutine(Movement());
    }

    // Starts and stops the movement
    public void SetMovement(bool value)
    {
        move = value;
    }

    // Returns the current position of vertical movement
    public Vector3 GetPos()
    {
        return pos;
    }

    // While move is true, move gameObject vertically
    IEnumerator Movement()
    {
        while (true)
        {
            if (move)
            {
                timer += Time.deltaTime;
                float y = Mathf.PingPong(speed * timer, amount);
                float newY = amount / 2 - y;
                pos = new Vector3(transform.position.x, newY, transform.position.z);
                transform.position = new Vector3(pos.x, transform.position.y + newY, pos.z);
            }
            yield return null;
        }
    }
}
