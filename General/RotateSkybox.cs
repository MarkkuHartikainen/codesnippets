﻿using UnityEngine;

// This script rotates the skybox

public class RotateSkybox : MonoBehaviour
{
    // The speed in witch skybox is rotated
    public float skyboxSpeed;

    void Update()
    {
        // Rotates the skybox with given speed
        RenderSettings.skybox.SetFloat("_Rotation", Time.time * skyboxSpeed);
    }
}
