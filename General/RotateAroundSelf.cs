﻿using UnityEngine;

// Rotates object around itself

public class RotateAroundSelf : MonoBehaviour
{
    // Amount of rotation in each axis
    public float x = 0;
    public float y = 80;
    public float z = 0;

    // Handles actual rotation
    protected void Update()
    {
        transform.Rotate(new Vector3(x, y, z) * Time.deltaTime);
    }
}
