﻿using System.Collections;
using UnityEngine;

// General movement script

public class Movement : MonoBehaviour
{
    // Movement speed of this gameObject
    public float movementSpeed = 1f;

    // Start movement coroutine to specific position
    public void MoveToPosition(Vector3 targetPos)
    {
        StopAllCoroutines();
        StartCoroutine(MoveToPos(targetPos));
    }
     
    // Move gameObject to target position with movement speed
    IEnumerator MoveToPos(Vector3 target)
    {
        while (Vector3.Distance(target, transform.position) >= 0.1f)
        {
            float step = movementSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target, step);
            yield return null;
        }
    }
}
