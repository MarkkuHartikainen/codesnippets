﻿using System;
using System.Collections;
using UnityEngine;

// This script handles all user inputs

// Struct for control data
public struct DirectionData
{
    public Direction direction;
}

// Different control directions
public enum Direction
{
    Up, Down, Left, Right, Tap, StopReceiving
}

public class ControlManager : MonoBehaviour
{
    // Touch start position
    private Vector2 fingerDownPosition;

    // Touch end position
    private Vector2 fingerUpPosition;

    // Minimum distance for finger to travel to detect touch as swipe
    private float minDistanceForSwipe = 200f;

    // Detect different touch screen swipe and D-pad direction inputs
    public static event Action<DirectionData> OnDirectionInput = delegate { };

    // Bool to detect and ignore slow swipes
    private bool slowSwipe = false;

    // Horizontal and vertical axis
    float x, y;

    // Bool to detect if user holds down keys
    bool holdingButtonDown;

    // At awake stop receiving input actions from previous scene
    void Awake()
    {
        StopReceivingInputs();
    }

    // Keep track of inputs
    private void Update()
    {
        // If there has been no touch screen touches, handle also keyboard inputs
        if (Input.touchCount == 0)
        {
            KeyboardControls();
        }
        
        // Handle touch screen controls
        TouchControls();

        // Handle D-pad controls
        DpadControls();
    }

    ////////////////////////////////////////////////////////// KEYBOARD CONTROLS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    
    void KeyboardControls()
    {
        // Pressing enter is same as tapping the screen or pressing middle button of the Android TV D-pad
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SendDirection(Direction.Tap);
        }
    }


    ////////////////////////////////////////////////////////// TOUCH CONTROLS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    // Touch controls handler
    void TouchControls()
    {
        // Return if there are no touches
        if (Input.touchCount <= 0)
        {
            return;
        }

        // Get current touch
        Touch t = Input.GetTouch(0);

        // When touch starts, save finger down position, and start timing slow swipe
        if (t.phase == TouchPhase.Began)
        {
            StopAllCoroutines();
            StartCoroutine(IgnoreSlowSwipe(1f));
            fingerUpPosition = t.position;
            fingerDownPosition = t.position;
        }

        // When touch ends detect the swipe
        if (t.phase == TouchPhase.Ended)
        {
            fingerDownPosition = t.position;
            DetectSwipe();
        }
    }

    // Detect swipe direction and lenght
    private void DetectSwipe()
    {
        // If swipe or touch is too slow, return
        if (slowSwipe)
        {
            return;
        }

        // If swipe distance is too short, detect touch as tap
        if (SwipeDistanceCheckMet())
        {         
            // Check if swipe is horizontal or vertical
            if (IsVerticalSwipe())
            {
                var direction = fingerDownPosition.y - fingerUpPosition.y > 0 ? Direction.Up : Direction.Down;
                SendDirection(direction);
            }
            else
            {
                var direction = fingerDownPosition.x - fingerUpPosition.x > 0 ? Direction.Left : Direction.Right;
                SendDirection(direction);
            }
            fingerUpPosition = fingerDownPosition;
        }
        else
        {
            var direction = Direction.Tap;
            SendDirection(direction);
        }
    }

    // Check if swipe is vertical
    private bool IsVerticalSwipe()
    {
        return VerticalMovementDistance() > HorizontalMovementDistance();
    }

    // Check if swipe is long enough
    private bool SwipeDistanceCheckMet()
    {
        return VerticalMovementDistance() > minDistanceForSwipe || HorizontalMovementDistance() > minDistanceForSwipe;
    }

    // Check amount of finger's vertical movement
    private float VerticalMovementDistance()
    {
        return Mathf.Abs(fingerDownPosition.y - fingerUpPosition.y);
    }

    // Check amount of finger's horizontal movement
    private float HorizontalMovementDistance()
    {
        return Mathf.Abs(fingerDownPosition.x - fingerUpPosition.x);
    }

    // Send swipe direction as action
    private void SendDirection(Direction dir)
    {
        DirectionData directionData = new DirectionData()
        {
            direction = dir
        };       
        OnDirectionInput(directionData);
    }

    // Detect and ignore slow swipes
    IEnumerator IgnoreSlowSwipe(float waitValue)
    {
        slowSwipe = false;
        yield return new WaitForSeconds(waitValue);
        slowSwipe = true;
    }



    ////////////////////////////////////////////////////////// ANDROID TV CONTROLLER D-PAD CONTROLS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    // Because there is no such thing as GetKeyDown on D-pad controls, it is needed to keep track when user stops pressing the D-pad direction (horizontal or vertical)
    private bool horizontalReset = true;
    private bool verticalReset = true;

    // Last "pressing amount" in horizontal and vertical directions. Again because there is no such thing as GetKeyDown on D-pad controls
    private float lastX = 0;
    private float lastY = 0;

    // Handles D-pad controls
   private void DpadControls()
   {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        // Middle circle button of the remote D-pad
        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            SendDirection(Direction.Tap);
        }

        // This bad boy is the custom GetKeyDown function in horizontal direction
        if (x > lastX && x > 0 && horizontalReset)
        {
            horizontalReset = false;
            var direction = Direction.Right;
            SendDirection(direction);
        }
        else if (x < lastX && x < 0 && horizontalReset)
        {
            horizontalReset = false;
            var direction = Direction.Left;
            SendDirection(direction);
        }
        else if (lastX > x && x > 0 || lastX < x && x < 0)
        {
            horizontalReset = true;
            holdingButtonDown = false;
        }

        // This bad boy is the custom GetKeyDown function in vertical direction
        if (y > lastY && y > 0 && verticalReset)
        {
            verticalReset = false;
            var direction = Direction.Up;
            SendDirection(direction);
        }
        else if (y < lastY && y < 0 && verticalReset)
        {
            verticalReset = false;
            var direction = Direction.Down;
            SendDirection(direction);
        }
        else if (lastY > y && y > 0 || lastY < y && y < 0)
        {
            verticalReset = true;
            holdingButtonDown = false;
        }

        // Start hold HoldingDown coroutine, when user holds down the button
        if (x == 1 && !holdingButtonDown)
        {
            StartCoroutine(HoldingDown(Direction.Right));    
        }
        else if (x == -1 && !holdingButtonDown)
        {
            StartCoroutine(HoldingDown(Direction.Left));
        }
        else if (y == 1 && !holdingButtonDown)
        {
            StartCoroutine(HoldingDown(Direction.Up));
        }
        else if (y == -1 && !holdingButtonDown)
        {
            StartCoroutine(HoldingDown(Direction.Down));
        }

        lastX = x;
        lastY = y;
    }

    // Send direction continually while user holds button down
    IEnumerator HoldingDown(Direction dir)
    {
        holdingButtonDown = true;
        SendDirection(dir);
        yield return new WaitForSeconds(0.15f);       
        if (x == 1 || x == -1 || y == 1 || y == -1)
        {          
            StartCoroutine(HoldingDown(dir));
        }
    }

    // Stops control manager from listening input actions
    public static void StopReceivingInputs()
    {
        OnDirectionInput = null;
    }
            
}

