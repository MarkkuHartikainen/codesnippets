﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;

// This script is the "brain" of the game. The manager of all managers. Behold the GAME MANAGER!
// Every single manager can be found and used via this script

public class GameManager : MonoBehaviour
{
    // StarManager of this game
    public static GameObject starManager;

    // StarPatternManager of this game
    public static GameObject starPatternManager;

    // MainCamera of this game
    public static GameObject mainCamera;

    // JunkManager of this game
    public static GameObject junkManager;

    // SolarSystem of this game
    public static GameObject solarSystem;

    // MetroRoute of this game
    public static GameObject metroRoute;

    // BlackHole of this game
    public static GameObject blackHole;

    // CutsceneManager of this game
    public static GameObject cutsceneManager;

    // GlobalVolume of this game
    public static GameObject globalVolume;

    // SettingsManager of this game
    private GameObject settingsManager;

    // MainCamera's current starOverview position
    public static Vector3 cameraPosition;

    // Storing the fake star IDs for spawning and deleting fake stars
    private static List<int> fakeStarIDs = new List<int>();

    // Bool marking the end of the game
    public static bool gameHasEnded = false;

    // Bool whether fake stars should be are allowed
    private static bool allowFakeStars = false;

    // Start listening the user inputs
    private void Awake()
    {
        // Game has just started, not ended
        gameHasEnded = false;

        // Find the managers and other GameObjects
        starManager = GameObject.Find("StarManager");
        mainCamera = GameObject.Find("MainCamera");
        junkManager = GameObject.Find("JunkManager");
        settingsManager = GameObject.Find("SettingsManager");
        starPatternManager = GameObject.Find("StarPatternManager");
        solarSystem = GameObject.Find("SolarSystem");
        metroRoute = GameObject.Find("MetroRoute");
        blackHole = GameObject.Find("BlackHole");
        cutsceneManager = GameObject.Find("CutsceneManager");
        globalVolume = GameObject.Find("GlobalVolume");
    }
    
    void Start()
    {
        // Inform the controlManager, that "I want to listen user inputs. Report them for me".
        ControlManager.OnDirectionInput += ControlHandler_OnDirectionInput;
    }

    // This handles the inputs from controlManager
    private void ControlHandler_OnDirectionInput(DirectionData data)
    {
        // If game has ended, every input leads to quit
        if (gameHasEnded)
        {
            print("Quit Game");
            Application.Quit();
        }

        if (!SettingsManager.pauseMenuOn)
        {
            // What happens with each input when pauseMenu is disabled
            switch (data.direction)
            {   
                // If pressing up or down, pauseMenu appears
                case Direction.Up:
                    settingsManager.GetComponent<SettingsManager>().PauseMenuAnimation(data.direction);
                    settingsManager.GetComponent<SettingsManager>().PauseMenu();
                    break;
                case Direction.Down:
                    settingsManager.GetComponent<SettingsManager>().PauseMenuAnimation(data.direction);
                    settingsManager.GetComponent<SettingsManager>().PauseMenu();
                    break;
                // If pressing left, destroy fake star aka. star not called from firebase
                case Direction.Left:
                    DestroyFakeStar();
                    break;
                // If pressing right, spawn fake star aka. star not called from firebase
                case Direction.Right:
                    if (allowFakeStars)
                    {
                        SpawnFakeStar();
                    }              
                    break;
                // If pressing D-pad center button, spawn fake star aka. star not called from firebase
                case Direction.Tap:
                    if (allowFakeStars)
                    {
                        SpawnFakeStar();
                    }
                    break;
                default:
                    break;
            }
        }
        else
        {
            // What happens with each input when pauseMenu is enabled
            switch (data.direction)
            {
                // Up and down buttons move the selector in pauseMenu
                case Direction.Up:
                    settingsManager.GetComponent<SettingsManager>().MoveSelector(-1);
                    break;
                case Direction.Down:
                    settingsManager.GetComponent<SettingsManager>().MoveSelector(1);
                    break;
                // Left, right and D-pad center buttons changes the values in pauseMenu
                case Direction.Left:
                    settingsManager.GetComponent<SettingsManager>().UseSelector(data.direction);
                    break;
                case Direction.Right:
                    settingsManager.GetComponent<SettingsManager>().UseSelector(data.direction);
                    break;
                case Direction.Tap:
                     settingsManager.GetComponent<SettingsManager>().UseSelector(data.direction);         
                    break;
                default:
                    break;
            }
        }
    }

    // Spawn fake star for testing purposes, with random name, random id and random color
    private void SpawnFakeStar()
    {
        string[] names = { "Maria", "Juhani", "Sofia", "Olavi", "Aurora", "Johannes", "Emilia", "Mikael", "Olivia", "Oliver", "Aino", "Ilmari", "Helmi", "Onni", "Matilda", "Elias", "Ilona", "Eino", "Amanda", "Matias" };
        string randomName = names[Random.Range(0, names.Length)];
        int randomID = Random.Range(5000, 50000);       
        fakeStarIDs.Add(randomID);
        Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        starManager.GetComponent<StarManager>().SpawnStar(randomID, randomName, color);
    }

    // Destroy random fake star for testing purposes
    private void DestroyFakeStar()
    {
        if (fakeStarIDs.Count == 0)
        {
            return;
        }

        int randomNum = Random.Range(0, fakeStarIDs.Count);
        starManager.GetComponent<StarManager>().DestroyStar(fakeStarIDs[randomNum]);
    }

    // Remove fake star from list of fake stars
    public static void RemoveFromFakeStars(int id)
    {
        fakeStarIDs.Remove(id);
    }

    // If fake stars are not allowed, destroy them
    public static void SetAllowFakeStars(float value)
    {
        switch (value)
        {
            case 0:
                allowFakeStars = false;
                while (fakeStarIDs.Count > 0)
                {
                    starManager.GetComponent<StarManager>().DestroyStar(fakeStarIDs[0]);
                }
                break;
            case 1:
                allowFakeStars = true;
                break;
            default:
                break;
        }     
    }

    // Updates MainCamera starOverview position and objects that are affected by that
    public static void UpdateCameraPosition(Vector3 newCamPos)
    {
        cameraPosition = newCamPos;

        // Update junk positions according to the new camera distance from stars
        junkManager.GetComponent<JunkManager>().UpdatePerimeter(cameraPosition);

        // Move metroStation according to the new camera distance from stars
        metroRoute.GetComponent<MetroRoute>().MoveToPosition(cameraPosition);

        // Move blackHole according to the new camera distance from stars
        blackHole.GetComponent<BlackHole>().MoveToPosition(cameraPosition);
    }

    // Calculate distance between two points, and return believable movement time between those points
    public static float DistanceToTime(Vector3 startPos, Vector3 targetPos)
    {
        float distance = Vector3.Distance(startPos, targetPos);
        return 3f + distance * 0.02f;
    }

    // LogOut -button functionality. If user is signed in, sign out of firebase. Game also ends after this.
    public static void LogOut()
    {
        var auth = FirebaseAuth.DefaultInstance;
        FirebaseUser user = auth.CurrentUser;
        if (user != null)
        {
            auth.SignOut();                      
            Debug.Log("Signed Out");
            //GoogleSignIn.DefaultInstance.SignOut();
        }
        gameHasEnded = true;
    }
}
