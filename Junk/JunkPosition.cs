﻿using UnityEngine;

// Junk position class for indicating whether thís position has already junk on it

public class JunkPosition
{
    // Position of this junk position
    public Vector3 position;

    // Is this position available for junk
    public bool available;

    // Create new junk position
    public JunkPosition(Vector3 newPos, bool isFree)
    {
        position = newPos;
        available = isFree;
    }
}