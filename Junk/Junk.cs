﻿using UnityEngine;

// This script defines junk rotation distance from stars and junk trail particle color

public class Junk : MonoBehaviour
{
    // Distance that junk rotates around the starObject
    public float rotationDistanceToStar = 0f;

    // Color of the junk trail particles
    [SerializeField]
    public Color32 mainColor = new Color(1f, 0.2352941f, 1f, 1f);

    // Return junk's rotation distance
    public float GetRotationDistance()
    {
        return rotationDistanceToStar;
    }

    // Return junk's trail particle color
    public Color GetMainColor()
    {
        return mainColor;
    }
}
