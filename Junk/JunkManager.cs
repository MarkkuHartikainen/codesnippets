﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Junk manager handles movement and position of the space junk objects

public class JunkManager : MonoBehaviour
{
    // Speed that moves junk manager to the center point of current star pattern
    public float movementSpeed = 1f;

    // Junk orbit distance from camera
    private float junkDistance = 6f;

    // Length between junk objects in the junk orbit
    public float junkInterval = 15f;
    
    // Holder for the junk gameObject
    public GameObject junkStorage;

    // List of all junk objects in the game
    public List<GameObject> junkCatalog = new List<GameObject>();

    // List of all junk storages in the scene
    private List<GameObject> listOfStorages = new List<GameObject>();

    // List of possible junk positions
    private List<JunkPosition> junkPositions = new List<JunkPosition>();

    // Radius of the junk orbit
    private float radius;

    // Perimeter length of the junk orbit
    private float perimeterLength;

    // Updates junk orbit according to the camera distance
    public void UpdatePerimeter(Vector3 cameraPos)
    {
        // Junk orbit radius is distance between center of the star pattern and camera minus junk distance
        float distance = Vector3.Distance(transform.position, cameraPos);
        radius = distance - junkDistance;
        perimeterLength = 2f * 3.14f * radius;

        // Calculating how many junk positions are needed to fill orbit with correct junk interval
        int neededStorage = Mathf.RoundToInt(perimeterLength / junkInterval);
        junkPositions.Clear();
        
        // Add junk positions to fill the orbit
        for (int i = 0; i < neededStorage; i++)
        {
            float angle = i * Mathf.PI * 2 / neededStorage;
            Vector3 pos = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * radius;
            pos += transform.position;
            junkPositions.Add(new JunkPosition(pos, true));
        }

        // Distribute junk positions for already existing junk
        foreach (GameObject storage in listOfStorages)
        {
            int selection = storage.GetComponent<JunkStorage>().TakeNearestPosition(junkPositions);
            if (selection >= 0)
            {
                junkPositions[selection].available = false;
            }           
        }

        // Create new junk for empty aka. available junk positions
        for (int i = 0; i < junkPositions.Count; i++)
        {
            if (junkPositions[i].available)
            {
                var newJunkStorage = Instantiate(junkStorage, junkPositions[i].position, transform.rotation);
                newJunkStorage.transform.parent = gameObject.transform;
                listOfStorages.Add(newJunkStorage);
                newJunkStorage.GetComponent<JunkStorage>().SpawnJunk(junkCatalog[Random.Range(0, junkCatalog.Count)]);
            }
        }

        // Destroy junk storages which have no junk position. Update list of junk storages.
        List<GameObject> tmpStorageList = new List<GameObject>();
        foreach (GameObject storage in listOfStorages)
        {
            if (storage.GetComponent<JunkStorage>().HasPosition())
            {
                tmpStorageList.Add(storage);
            }
        }
        listOfStorages.Clear();
        listOfStorages = tmpStorageList;
    }

    // Move to star pattern center
    public void UpdateCenter(Vector3 newCenter)
    {
        StopAllCoroutines();
        StartCoroutine(MoveToPosition(newCenter));
    }

    // Remove item from junk storages
    public void RemoveFromStorages(GameObject itemToRemove)
    {
        listOfStorages.Remove(itemToRemove);
    }

    // Choose nearest junk for spawning star, remove that junk storage and update junk perimeter
    public void ChooseJunk(GameObject starObject)
    {
        GameObject closestStorage = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 cameraPos = GameManager.mainCamera.transform.position;

        foreach (GameObject storage in listOfStorages)
        {
            Vector3 directionToTarget = storage.transform.position - cameraPos;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                closestStorage = storage;
            }
        }
       
        RemoveFromStorages(closestStorage);
        if (listOfStorages.Count <= 0)
        {
            UpdatePerimeter(GameManager.cameraPosition);
        }
        closestStorage.transform.parent = starObject.transform;
        closestStorage.GetComponent<JunkStorage>().AttachToStar(starObject);
    }

    // Set new junk distance
    public void SetJunkDistance(float value)
    {
        junkDistance = value;
        UpdatePerimeter(GameManager.cameraPosition);
    }

    // Move junkManager to target position and update junk perimeter
    IEnumerator MoveToPosition(Vector3 target)
    {
        while (Vector3.Distance(target, transform.position) >= 0.1f)
        {
            float step = movementSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target, step);
            yield return null;
        }

        UpdatePerimeter(GameManager.cameraPosition);
    }
}
