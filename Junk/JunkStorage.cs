﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Junk storage is parent for a junk object and handles junk's particle effects and animations

public class JunkStorage : MonoBehaviour
{ 
    // Storage's position
    private Vector3 myPosition = new Vector3(0f, 0f, 0f);

    // Does storage have position in the junk orbit?
    private bool hasOrbitPosition = true;

    // Junk's rotation distance from star
    private float radiusFromStar = 0f;

    // Actual parent and junk holder part of the storage
    public GameObject junkHolder;

    // Junk storage's particle system
    public GameObject trailParticleSystem;

    // Movement speed while attaching to star
    public float speed = 15f;

    // Rotation speed around star
    public float starRotationSpeed = 2f;

    // Animator component
    private Animator animator;

    // Junk storage's star object to attach
    private Transform myStar = null;

    // Is junk storage attached to a star?
    private bool attachedToStar = false;

    // Movement coroutine
    IEnumerator movementCoroutine = null;

    // Play spawn aka. confetti animation at start
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetInteger("SpawnAnimationID", Random.Range(0, 2));
    }

    // If star object is given, move towards star and rotate around it
    void Update()
    {
        if (attachedToStar == false)
        {
            if (myStar != null)
            {
                MoveTowardsStar(myStar);
            }
        }
        else
        {
            transform.RotateAround(myStar.position, Vector3.up, starRotationSpeed * Time.deltaTime);
        }
    }

    // Calculate nearest available junk position and start movement coroutine
    public int TakeNearestPosition(List<JunkPosition> listOfPositions)
    {
        float shortestDistance = Mathf.Infinity;
        int posIndex = -1;

        for (int i = 0; i < listOfPositions.Count; i++)
        {
            if (listOfPositions[i].available)
            {
                float distance = Vector3.Distance(listOfPositions[i].position, transform.position);
                if (distance < shortestDistance)
                {
                    shortestDistance = distance;
                    myPosition = listOfPositions[i].position;
                    posIndex = i;                  
                }
            }           
        }

        if (posIndex < 0)
        {
            hasOrbitPosition = false;
        }
        else
        {
            hasOrbitPosition = true;
            movementCoroutine = MoveToPosition(myPosition, GameManager.DistanceToTime(transform.position, myPosition));
            StartCoroutine(movementCoroutine);           
        }

        return posIndex;
    }

    // Return true if this storage has junk position, else destroy
    public bool HasPosition()
    {
        if (!hasOrbitPosition)
        {
            DestroyStorage();
        }      
        return hasOrbitPosition;
    }

    // Spawn junk on this storage position
    public void SpawnJunk(GameObject junkPrefab)
    {
        var newJunk = Instantiate(junkPrefab, transform.position, transform.rotation);
        newJunk.transform.parent = junkHolder.transform;
        ChangeTrailColor(junkPrefab.GetComponent<Junk>().GetMainColor());
        radiusFromStar = newJunk.GetComponent<Junk>().GetRotationDistance();     
    }

    // Change junk trail color
    private void ChangeTrailColor(Color newColor)
    {
        foreach (Transform child in trailParticleSystem.transform)
        {
            var trailPS = child.GetComponent<ParticleSystem>().main;
            trailPS.startColor = newColor;
        }
    }

    // Play destroy animation and destroy this storage
    public void DestroyStorage()
    {
        animator.SetInteger("LeaveAnimationID", Random.Range(0, 2));
        StartCoroutine(Destroy(3f));
    }

    // Save star object which to attach
    public void AttachToStar(GameObject star)
    {
        if (movementCoroutine != null)
        {
            StopCoroutine(movementCoroutine);
        }
        star.GetComponent<Star>().SetMyJunkStorage(gameObject);
        myStar = star.transform;
    }

    // Reach the star object and destroy this storage under 20 seconds
    public void MoveTowardsStar(Transform star)
    {
        if (Vector3.Distance(transform.position, star.transform.position) > radiusFromStar)
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, star.transform.position, step);
        }
        else
        {
            attachedToStar = true;
            Invoke("DestroyStorage", Random.Range(12, 20));
        }
     
        // Y-position correction for the star attachment phase
        if (transform.localPosition.y > 0.01 || transform.localPosition.y < -0.01)
        {
            Vector3 heightCorrection = new Vector3(transform.localPosition.x, Mathf.Lerp(transform.localPosition.y, 0, Time.deltaTime), transform.localPosition.z); 
            transform.localPosition = heightCorrection;
        }      
    }

    // Move to a new storage position in certain time/duration
    IEnumerator MoveToPosition(Vector3 target, float duration)
    {
        float counter = 0;

        Vector3 startPos = transform.position;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            transform.position = Vector3.Lerp(startPos, target, counter / duration);
            yield return null;
        }

        transform.position = target;
    }

    // Destroy this gameObject after certain wait time
    IEnumerator Destroy(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);      
        Destroy(gameObject);
    }
}
