﻿using System.Collections.Generic;
using UnityEngine;

// Simplified version of the junk manager for main menu

public class JunkManagerMenu : MonoBehaviour
{
    // Length between junk objects in the junk orbit
    public float junkInterval = 15f;

    // Holder for the junk gameObject
    public GameObject junkStorage;

    // List of all junk objects in the game
    public List<GameObject> junkCatalog = new List<GameObject>();

    // List of all junk storages in the scene
    private List<GameObject> listOfStorages = new List<GameObject>();

    // List of possible junk positions
    private List<JunkPosition> junkPositions = new List<JunkPosition>();

    // Radius of the junk orbit
    public float radius = 5f;

    // Perimeter length of the junk orbit
    private float perimeterLength;

    // Start by making junk in the scene
    void Start()
    {
        UpdatePerimeter();
    }

    // Updates junk orbit according to the given radius
    public void UpdatePerimeter()
    {
        // Calculating how many junk positions are needed to fill orbit with correct junk interval
        perimeterLength = 2f * 3.14f * radius;
        int neededStorage = Mathf.RoundToInt(perimeterLength / junkInterval);
        junkPositions.Clear();

        // Add junk positions to fill the orbit
        for (int i = 0; i < neededStorage; i++)
        {
            float angle = i * Mathf.PI * 2 / neededStorage;
            Vector3 pos = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * radius;
            pos += transform.position;
            junkPositions.Add(new JunkPosition(pos, true));
        }

        // Distribute junk positions 
        foreach (GameObject storage in listOfStorages)
        {
            int selection = storage.GetComponent<JunkStorage>().TakeNearestPosition(junkPositions);
            if (selection >= 0)
            {
                junkPositions[selection].available = false;
            }           
        }

        // Create new junk for empty aka. available junk positions
        for (int i = 0; i < junkPositions.Count; i++)
        {
            if (junkPositions[i].available)
            {
                var newJunkStorage = Instantiate(junkStorage, junkPositions[i].position, transform.rotation);
                newJunkStorage.transform.parent = gameObject.transform;
                listOfStorages.Add(newJunkStorage);
                newJunkStorage.GetComponent<JunkStorage>().SpawnJunk(junkCatalog[Random.Range(0, junkCatalog.Count)]);
            }
        }

        // Destroy junk storages which have no junk position. Update list of junk storages.
        List<GameObject> tmpStorageList = new List<GameObject>();
        foreach (GameObject storage in listOfStorages)
        {
            if (storage.GetComponent<JunkStorage>().HasPosition())
            {
                tmpStorageList.Add(storage);
            }
        }
        listOfStorages.Clear();
        listOfStorages = tmpStorageList;
    }

    // Remove item from junk storages
    public void RemoveFromStorages(GameObject itemToRemove)
    {
        listOfStorages.Remove(itemToRemove);
    }

}
