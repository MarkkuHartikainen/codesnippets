﻿using System.Collections;
using UnityEngine;

// This script moves the black hole and keeps it in the center of the screen

public class BlackHole : MonoBehaviour
{
    // Swirl part of the effect
    public GameObject swirls;

    // Particle system of the black hole
    private ParticleSystem swirlParticle;

    // Movement speed of the black hole
    public float movementSpeed = 0.5f;

    // Change simulation speed of the black hole to minimal
    void Start()
    {
        swirlParticle = swirls.GetComponent<ParticleSystem>();
        var main = swirlParticle.main;
        main.simulationSpeed = 0.1f;
    }

    // Start movement coroutine to keep black hole in the center of the star pattern and the screen
    public void MoveToPosition(Vector3 target)
    {
        StopAllCoroutines();
        StartCoroutine(MoveToPos(target));
    }

    // Move black hole to target position
    IEnumerator MoveToPos(Vector3 target)
    {
        Vector3 blackHoleTarget = new Vector3(target.x, target.y, transform.position.z);

        while (Vector3.Distance(blackHoleTarget, transform.position) >= 0.1f)
        {
            float step = movementSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, blackHoleTarget, step);
            yield return null;
        }
    }
}
