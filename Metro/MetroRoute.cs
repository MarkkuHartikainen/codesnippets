﻿using System.Collections;
using UnityEngine;
using TMPro;

// This script handles the metro station position and animation

public class MetroRoute : MonoBehaviour
{
    // Metro station movement speed
    public float movementSpeed = 1f;

    // Minimum and maximum idle time aka. the time metro spents in the station
    public float minIdleTime = 10f;
    public float maxIdleTime = 30f;

    // Metro arrival animation interval
    private float metroInterval = 60f;

    // Metro's face. Needed for camera to know what part of the metro to shoot
    public GameObject metroFace;

    // Position offset of the metro station
    public Vector3 posOffset = new Vector3(0f, -4f, 0f);

    // Current amount of stars/users
    public TMP_Text userCount;

    // Animator component
    private Animator animator;

    // Coroutine for the movement
    private IEnumerator coroutine;

    // Start timing metro animation and give metro's face transfrom for the main camera 
    void Start()
    {
        animator = GetComponent<Animator>();
        GameManager.mainCamera.GetComponent<MainCamera>().SetMetroTarget(metroFace.transform);
        StartCoroutine(AnimationHandler());
    }

    // Move to certain position
    public void MoveToPosition(Vector3 newPos)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }  
        coroutine = MoveToPos(newPos + posOffset);
        StartCoroutine(coroutine);
    }

    // Called directly from metro arrival animation. This function sends main camera information, that the metro is arriving and needs attention
    public void GetCameraFocus()
    {
        GameManager.mainCamera.GetComponent<MainCamera>().SetCameraState(MainCamera.CameraState.metroArrival);
    }

    // Called directly from metro arrival animation. If main camera is not doing anything more important, play metro cutscene
    public void PlayCutscene()
    {
        if (GameManager.mainCamera.GetComponent<MainCamera>().GetCurrentCameraState() == MainCamera.CameraState.metroArrival)
        {
            GameManager.cutsceneManager.GetComponent<CutsceneManager>().PlayRandomCutscene();
        }      
    }

    // Update star amount / user amount canvas
    public void SetUserCount(int newCount)
    {
        userCount.text = newCount.ToString();
    }

    // Set new wait time aka. interval between metro arrivals
    public void SetMetroInterval(float value)
    {
        metroInterval = value;
    }

    // Spawn metro back to the metro station and start animation coroutine again
    public void ResetAnimation()
    {
        StopAllCoroutines();
        animator.StopPlayback();
        animator.Play("Idle", 0, 0f);
        StartCoroutine(AnimationHandler());
    }

    // Move metro station to any position and stop the up and down movement during the process
    IEnumerator MoveToPos(Vector3 target)
    {
        GetComponent<UpAndDown>().SetMovement(false);
        Vector3 correctedTarget = new Vector3(target.x, target.y + GetComponent<UpAndDown>().GetPos().y, target.z);
        while (Vector3.Distance(correctedTarget, transform.position) >= 0.1f)
        {          
            transform.position = Vector3.MoveTowards(transform.position, correctedTarget, movementSpeed * Time.deltaTime);
            yield return null;
        }
        transform.position = correctedTarget;
        GetComponent<UpAndDown>().SetMovement(true);
    }

    // Metro animation handler
    IEnumerator AnimationHandler()
    {
        // Time spent on the metro station before departure
        float idleTimer = Random.Range(minIdleTime, maxIdleTime);
        while (idleTimer > 0)
        {
            idleTimer -= Time.deltaTime;
            yield return null;
        }

        animator.SetTrigger("MetroDeparture");

        // Time waiting for next metro arrival animation. There is 20 seconds uncertainty about the right arrival time.
        float arrivalTimer = metroInterval + Random.Range(-10f, 10);
        while (arrivalTimer > 0)
        {
            arrivalTimer -= Time.deltaTime;
            yield return null;
        }

        // Finally metro arrives again
        animator.SetTrigger("MetroArrive");
        float resetTimer = animator.GetCurrentAnimatorStateInfo(0).length;
        while (resetTimer > 0)
        {
            resetTimer -= Time.deltaTime;
            yield return null;
        }

        // When animation is finished, start animation and metro cycle again
        StartCoroutine(AnimationHandler());
    }
}
