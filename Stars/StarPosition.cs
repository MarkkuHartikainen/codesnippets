﻿using System.Collections.Generic;
using UnityEngine;

// Information about each star position

public class StarPosition : MonoBehaviour
{
    // Each star position has its own id, position, star connection amount, list of neighborstars and star-gameObject

    // ID of this star position
    [HideInInspector]
    public int id;

    // Position of this star position
    [HideInInspector]
    public Vector3 position;

    // Amount of star connections in this star position (0, 1 or 2)
    public List<GameObject> starConnections;

    // Neighbor stars of this star position aka. where to draw lines
    [HideInInspector]
    public List<GameObject> neighbourStars;

    // Current star -gameObject of this position
    [HideInInspector]
    public GameObject starObject;

    // Star position id is determined by gameObjects position in the inspector hierarchy
    void Awake()
    {
        id = transform.GetSiblingIndex();
        position = transform.position;
    }

    // Set star-gameObject for this star position
    public void SetStar(GameObject newStarObject)
    {
        starObject = newStarObject;
    }

    // Set neighbor stars for this star position
    public void SetNeighbours(List<GameObject> newNeighbours)
    {
        neighbourStars = newNeighbours;
    }
}
