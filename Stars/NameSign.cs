﻿using UnityEngine;

// This script makes gameObject always look at the main camera

public class NameSign : MonoBehaviour
{
    // Transfrom to look at
    private Transform cam;
    
    // Turning speed of the gameObject
    private float lookAtSpeed = 5;

    // Find the target transform
    void Start()
    {
        cam = GameManager.mainCamera.transform;
    }

    // Look at the target transform with lookAtSpeed
    void LateUpdate()
    {
        var n = cam.transform.position - transform.position;
        var newRotation = Quaternion.LookRotation(n) * Quaternion.Euler(0, -180, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * lookAtSpeed);
    }
}
