﻿using UnityEngine;

// Manager of star idle animations

public class StarIdleManager : StateMachineBehaviour
{
    // Minimum and maximum wait time between star idle animations
    private float minTimeBetweenAnimations = 1f;
    private float maxTimeBetweenAnimations = 12f;

    // Timer for counting
    private float timer = 0f;

    // Is the idle animation playing?
    private bool isPlaying = false;

    // Once entering this state, idle animation has stopped
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetInteger("IdleAnimationID", -1);
        isPlaying = false;
    }

    // Play random idle animation once timer has run out
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (isPlaying == false)
        {
            if (timer <= 0)
            {
                isPlaying = true;
                timer = Random.Range(minTimeBetweenAnimations, maxTimeBetweenAnimations);
                animator.SetInteger("IdleAnimationID", Random.Range(0, 19));              
            }
            else
            {
                timer -= Time.deltaTime;
            }
        }
    }

}
