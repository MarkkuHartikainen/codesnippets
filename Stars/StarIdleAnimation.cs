﻿using UnityEngine;

// This script resets the star idle animation after completion

public class StarIdleAnimation : StateMachineBehaviour
{
    // Timer for resetting purposes
    private float timer = 11f;

    // When idle animation starts set timer to 11 (according to the longest possible idle -animation)
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 11f;
    }

    // Once timer has run out, reset the idle animation to start new one
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (timer <= 0f)
        {
            timer = 11f;
            animator.SetInteger("IdleAnimationID", -1);
            animator.SetTrigger("ResetIdle");
        }
        else
        {
            timer -= Time.deltaTime;
        }
    }
}
