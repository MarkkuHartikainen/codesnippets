﻿using System.Collections.Generic;
using UnityEngine;

// StarManager handles all the star group behaviors

public class StarManager : MonoBehaviour
{
    // Maximum amount of stars in the level
    private float maxStarAmount = 0;

    // Lifespan of the stars
    private float starLifespan = 0;

    // Spawnable star -prefab
    public GameObject starPrefab;

    // List of all star -gameobjects in the scene
    public static List<GameObject> listOfStars = new List<GameObject>();

    // List of currently spawning stars in the scene
    private List<GameObject> spawningStars = new List<GameObject>();

    // List of self destructing stars in the scene
    private List<GameObject> selfDestructingStars = new List<GameObject>();

    // List of all star data
    public static List<StarData> listOfStarData = new List<StarData>();

    // Random spawn position for a spawning star
    private Vector3 randomSpawnOffset = new Vector3(0f, 0f, 0f);

    // Gap between multiple spawning stars and vector for it
    public float spawnGap = 7f;
    private Vector3 spawnGapVector = new Vector3(0f, 0f, 0f);

    // Default color of the stars
    public static Color defaultStarColor = new Color(1f, 0.2352941f, 1f, 1f);

    // Invisible rectangle around all the spawned stars
    private Bounds bounds;

    // Star which condition has been most recently modified, and which is the camera's main focus
    private GameObject focusStar;

    // Spawn star with certain id, name and color
    public void SpawnStar(int id, string name, Color color)
    {       
        // Make sure that star with this id doesn't already exist
        foreach (StarData data in listOfStarData)
        {
            if (data.id == id)
            {
                return;
            }
        }

        // If there is already max amount of stars, remove oldest star before spawning
        if (listOfStars.Count >= maxStarAmount)
        {
            GameManager.RemoveFromFakeStars(listOfStars[0].GetComponent<Star>().myData.id);
            selfDestructingStars.Add(listOfStars[0]);
            listOfStars[0].GetComponent<Star>().Leave(true);
            spawningStars.Remove(listOfStars[0]);   
            listOfStars.RemoveAt(0);
            listOfStarData.RemoveAt(0);           
        }

        // Stop the possible cutscene
        GameManager.cutsceneManager.GetComponent<CutsceneManager>().StopPlaying();

        // Create new random spawn position, if there isn't multiple spawning stars
        if (spawningStars.Count <= 0)
        {
            randomSpawnOffset = new Vector3(Random.Range(0, 10), Random.Range(0, 8), Random.Range(-40, -20));        
        }
        
        // If there is already one or more spawning stars, just add spawn gap between them
        Vector3 spawnPos = GameManager.cameraPosition + randomSpawnOffset + spawnGapVector;

        // Instantiate the star prefab, set it's data and lifespan
        var myNewStar = Instantiate(starPrefab, spawnPos, transform.rotation);
        myNewStar.transform.parent = gameObject.transform;
        myNewStar.GetComponent<Star>().SetData(new StarData(id, name, myNewStar, color));
        myNewStar.GetComponent<Star>().SetStarLifespan(starLifespan);

        // Add spawning star to list of spawning stars, increase the spawn gab between stars, and set spawning star as camera's focus
        spawningStars.Add(myNewStar);
        spawnGapVector.x += spawnGap;
        focusStar = myNewStar;

        // Move stars to their positions
        MoveStarsToPositions();

        // Choose junk for this spawning star
        GameManager.junkManager.GetComponent<JunkManager>().ChooseJunk(myNewStar);

        // Calculate the center point of the spawning stars, set camera state to starSpawn, and update the user amount sign
        GameManager.mainCamera.GetComponent<MainCamera>().SetSpawnCenter(spawningStars);
        GameManager.mainCamera.GetComponent<MainCamera>().SetCameraState(MainCamera.CameraState.starSpawn);
        GameManager.metroRoute.GetComponent<MetroRoute>().SetUserCount(listOfStars.Count);
    }

    // Destroys star with certain id
    public void DestroyStar(int id)
    {
        // Make sure that there is star to destroy with this id
        foreach (StarData data in listOfStarData)
        {
            if (data.id == id)
            {
                // Stop possible cutscene
                GameManager.cutsceneManager.GetComponent<CutsceneManager>().StopPlaying();

                // Find star with this id from the list of stars, and set it as focus star
                int posInList = listOfStars.IndexOf(data.starObject);
                focusStar = listOfStars[posInList];

                // Check if leaving star is also spawning star (that would be fast but possible)
                if (spawningStars.Contains(listOfStars[posInList]))
                {
                    spawningStars.Remove(listOfStars[posInList]);
                    listOfStars[posInList].GetComponent<Animator>().StopPlayback();
                }
                
                // Zoom main camera to leaving star
                GameManager.mainCamera.GetComponent<MainCamera>().ZoomInToPos(listOfStars[posInList].transform);
                GameManager.mainCamera.GetComponent<MainCamera>().SetCameraState(MainCamera.CameraState.zoomIn);

                // Start leaving process of the leaving star
                listOfStars[posInList].GetComponent<Star>().Leave(false);

                // Remove this star from any possible list
                GameManager.RemoveFromFakeStars(listOfStars[posInList].GetComponent<Star>().myData.id);
                listOfStars.RemoveAt(posInList);
                listOfStarData.RemoveAt(posInList);

                // Update the user amount sign
                GameManager.metroRoute.GetComponent<MetroRoute>().SetUserCount(listOfStars.Count);
                break;
            }
        }
    }

    // Destroys star with certain id, but with self destruct settings
    public void SelfDestructStar(int id)
    {
        // Make sure that there is star to destroy with this id
        foreach (StarData data in listOfStarData)
        {
            if (data.id == id)
            {
                // Find star with this id from the list of stars
                int posInList = listOfStars.IndexOf(data.starObject);

                // Check if leaving star is also spawning star (that would be fast but possible)
                if (spawningStars.Contains(listOfStars[posInList]))
                {
                    spawningStars.Remove(listOfStars[posInList]);
                    listOfStars[posInList].GetComponent<Animator>().StopPlayback();
                }

                // Self destruction is not so important event, but if there is no cutscene running, ongoing spawn process or metro arriving, shift camera focus to this self destructing star  
                if (!GameManager.cutsceneManager.GetComponent<CutsceneManager>().GetIsPlaying() && spawningStars.Count <= 0 && GameManager.mainCamera.GetComponent<MainCamera>().GetCurrentCameraState() != MainCamera.CameraState.metroArrival)
                {
                    focusStar = listOfStars[posInList];
                    GameManager.mainCamera.GetComponent<MainCamera>().ZoomInToPos(listOfStars[posInList].transform);
                    GameManager.mainCamera.GetComponent<MainCamera>().SetCameraState(MainCamera.CameraState.zoomIn);
                }

                // Start leaving process of the self destructing star
                listOfStars[posInList].GetComponent<Star>().Leave(true);

                // Remove this star from any possible list
                GameManager.RemoveFromFakeStars(listOfStars[posInList].GetComponent<Star>().myData.id);
                listOfStars.RemoveAt(posInList);
                listOfStarData.RemoveAt(posInList);

                // Update the user amount sign
                GameManager.metroRoute.GetComponent<MetroRoute>().SetUserCount(listOfStars.Count);
                break;
            }
        }
    }

    // Move all stars to their positions if there isn't ongoing self destructing process
    public void MoveStarsToPositions()
    {
        if (listOfStars.Count > 0 && selfDestructingStars.Count <= 0)
        {
            // Distribute star pattern positions for every star in the list of stars
            GameManager.starPatternManager.GetComponent<StarPatternManager>().SetStarPositions(listOfStars);

            // Move every star to its star pattern position, if the star isn't doing spawn animation
            for (int i = 0; i < listOfStars.Count; i++)
            {
                if (!spawningStars.Contains(listOfStars[i]))
                {
                    listOfStars[i].GetComponent<Star>().MoveToStarPosition();
                }               
            }

            // Update the invisible rectangle around spawned stars
            UpdateStarBounds();
        }     
    }

    // Double check that there is no spawning stars waiting for camera focus
    public void CheckSpawningStars()
    {
        if (spawningStars.Count > 0)
        {
            focusStar = spawningStars[spawningStars.Count - 1];
            GameManager.mainCamera.GetComponent<MainCamera>().SetSpawnCenter(spawningStars);
            GameManager.mainCamera.GetComponent<MainCamera>().SetCameraState(MainCamera.CameraState.starSpawn);
        }
        else
        {
            GameManager.mainCamera.GetComponent<MainCamera>().SetCameraState(MainCamera.CameraState.starOverview);
        }
    }

    // Remove star from list of self destructing stars
    public void RemoveFromSelfDestructingStars(GameObject star)
    {
        selfDestructingStars.Remove(star);
        MoveStarsToPositions();
    }

    // Disable linerenderers for every single star
    public void DisableLineRenderers()
    {
        for (int i = 0; i < listOfStars.Count; i++)
        {
            listOfStars[i].GetComponent<Star>().UseLineRenderer(0, false);
        }
    }

    // Returns the current focus star
    public GameObject GetFocusStar()
    {
        return focusStar;
    }

    // Remove star from list of spawning stars
    public void RemoveFromSpawningStars(GameObject starToRemove)
    {
        spawningStars.Remove(starToRemove);
        
        // If this star was the last spawning star and focus star, make camera follow this star
        if (spawningStars.Count == 0 && starToRemove == GetFocusStar())
        {
            GameManager.mainCamera.GetComponent<MainCamera>().FollowTarget(starToRemove.transform);
            spawnGapVector = new Vector3(0f, 0f, 0f);
        }
        else
        {
            // Else calculate the center of the spawning stars again
            GameManager.mainCamera.GetComponent<MainCamera>().SetSpawnCenter(spawningStars);
        }    
    }

    // Trigger spawn animation of the spawning stars
    public void TriggerSpawnAnimation()
    {
        foreach (var star in spawningStars)
        {
            star.GetComponent<Star>().PlaySpawnAnimation();
        }
    }

    // If every star has reached its star pattern position, activate line renderers
    public void ReadyToDraw()
    {
        for (int i = 0; i < listOfStars.Count; i++)
        {
            if (listOfStars[i].transform.position != listOfStars[i].GetComponent<Star>().myData.position)
            {
                return;
            }            
        }

        for (int i = 0; i < listOfStars.Count; i++)
        {
            listOfStars[i].GetComponent<Star>().DrawConnections();
        }
    }

    // Converts Hex color to RGB-color
    public Color RGBConverter(string hexText)
    {
        Color result = new Color(1f, 0.2352941f, 1f, 1f);
        hexText = hexText.Replace(" ", "");
        if (!hexText.StartsWith("#")) { hexText = hexText.Insert(0, "#"); }

        Color rgbColor;
        if (ColorUtility.TryParseHtmlString(hexText, out rgbColor))
        {
            result = rgbColor;
        }

        return result;
    }

    // Draws invisible square aka. bounds around all the spawned stars (using their final star pattern position)
    public void UpdateStarBounds()
    {
        bounds = new Bounds(new Vector3(0f, 0f, 0f), Vector3.zero);
        for (int i = 0; i < listOfStars.Count; i++)
        {
            bounds.Encapsulate(listOfStars[i].GetComponent<Star>().myData.position);
        }      
        
        // Move junkManager to the center of this square (to keep it centered)
        GameManager.junkManager.GetComponent<JunkManager>().UpdateCenter(bounds.center);

        // Update planet positions according to this square (to prevent planets from colliding with stars)
        GameManager.solarSystem.GetComponent<SolarSystem>().UpdatePlanetPositions(bounds.center, bounds.max.y, bounds.min.y);
    }

    // Set max star amount value and destroy stars that exceed that value
    public void SetMaxStarAmount(float value)
    {
        maxStarAmount = value;

        while (listOfStars.Count > maxStarAmount)
        {
            SelfDestructStar(listOfStars[0].GetComponent<Star>().myData.id);
        }
    }

    // Set star lifespan value
    public void SetStarLifespan(float value)
    {
        starLifespan = value;
        foreach (GameObject star in listOfStars)
        {
            star.GetComponent<Star>().SetStarLifespan(starLifespan);
        }
    }

    // Return current star bounds
    public Bounds GetBounds()
    {
        return bounds;
    }
}
