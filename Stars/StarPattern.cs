﻿using System.Collections.Generic;
using UnityEngine;

// Star pattern holds information about the star positions. In addition, size of the star pattern affects the main camera movement speed

public class StarPattern : MonoBehaviour
{
    // Star positions of this star pattern
    [HideInInspector]
    public List<StarPosition> starPositions;

    // Preferred camera movement and rotation speed while showing this star pattern
    public float cameraSmoothAmount = 0.5f;
    public float cameraBackwardSpeed = 2f;
    public float cameraRotationSpeed = 0.6f;

    // Find and store the star positions of this pattern
    void Start()
    {
        foreach (Transform child in transform)
        {
            starPositions.Add(child.GetComponent<StarPosition>());
        }
    }

    // Return the preferred camera smooth movement amount
    public float GetSmoothAmount()
    {
        return cameraSmoothAmount;
    }

    // Return the preferred camera backward movement speed
    public float GetBackwardSpeed()
    {
        return cameraBackwardSpeed;
    }

    // Return the preferred camera rotation speed
    public float GetRotationSpeed()
    {
        return cameraRotationSpeed;
    }
}
