﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.VFX;

// This script handles the individual star behaviors

public class Star : MonoBehaviour
{   
    // Star's name text
    public TMP_Text nameText;

    // Star's star data
    [HideInInspector]
    public StarData myData;

    // Z-axis offset of the line renderers
    private float connectionOffset = 2f;

    // Each star has two line renderers to draw their portion of the star pattern
    public GameObject lineRendererOne;
    public GameObject lineRendererTwo;
    LineRenderer lineRendOne;
    LineRenderer lineRendTwo;

    // List of this star's line renderers
    List<LineRenderer> lineRenderers = new List<LineRenderer>();

    // Portal, fireworks and star exploding particle effect containers
    public GameObject portal;
    public GameObject fireworks;
    VisualEffect fireworkEffect;
    public GameObject starBoom;
   
    // Star's animator
    private Animator animator;

    // The junk storage object which star gets during spawn
    private GameObject myJunkStorage;
    
    // Coroutine for moving
    IEnumerator movementCoroutine = null;

    // Coroutine which handles the star's lifespan
    IEnumerator lifespanCoroutine = null;

    // Find components and put them on default state
    void Awake()
    {    
        animator = GetComponent<Animator>();
        animator.Play("Spawn");     
        nameText.GetComponent<Animator>().Play("Spawn");
        nameText.gameObject.SetActive(false);

        fireworkEffect = fireworks.GetComponent<VisualEffect>();
        lineRendOne = lineRendererOne.GetComponent<LineRenderer>();
        lineRendTwo = lineRendererTwo.GetComponent<LineRenderer>();

        lineRenderers.Add(lineRendOne);
        lineRenderers.Add(lineRendTwo);
        UseLineRenderer(0, false);
        SetPortal(false);
    }

    // Plays the star spawn animation
    public void PlaySpawnAnimation()
    {
        if (animator.GetInteger("SpawnAnimationID") == -1)
        {
            StartCoroutine(SpawnAnimation());
        }   
    }

    // Makes the star leave the star pattern. Bool marking whether leaving is caused by self destruct
    public void Leave(bool selfDestruct)
    {
        StopAllCoroutines();
        StartCoroutine(LeaveAnimation(selfDestruct));
    }

    // Set lifespan of this star and start lifespan coroutine if lifespan isn't infinite
    public void SetStarLifespan(float lifespan)
    {
        if (lifespanCoroutine != null)
        {
            StopCoroutine(lifespanCoroutine);
        }

        if (lifespan > 20)
        {
            return;
        }

        lifespanCoroutine = LifespanHandler(lifespan);
        StartCoroutine(lifespanCoroutine);
    }

    // Sets this star's junk storage
    public void SetMyJunkStorage(GameObject newStorage)
    {
        myJunkStorage = newStorage;
    }

    // Give StarData for this star
    public void SetData(StarData data)
    {
        myData = data;
        nameText.text = myData.name;
        SetColor(data.color);
    }

    // Change star, portal, portal particles and name text to a certain color
    public void SetColor(Color color)
    {
        var starRenderer = transform.GetChild(0).GetComponent<Renderer>();
        starRenderer.material.SetColor("_Color", color);

        var portalRenderer = portal.GetComponent<Renderer>();
        portalRenderer.material.SetColor("_Color", color);

        var portalParticeSystem = portal.transform.GetChild(0).GetComponent<ParticleSystem>().main;
        portalParticeSystem.startColor = color;

        foreach (Transform child in starBoom.transform)
        {
            var starBoomPS = child.GetComponent<ParticleSystem>().main;
            starBoomPS.startColor = color;
        }

        nameText.color = color;
        nameText.fontMaterial.SetColor(ShaderUtilities.ID_GlowColor, color);
    }

    // Move star to star pattern position
    public void MoveToStarPosition()
    {
        if (movementCoroutine != null)
        {
            StopCoroutine(movementCoroutine);
        }
        movementCoroutine = MoveToStarPosition(GameManager.DistanceToTime(transform.position, myData.position));
        StartCoroutine(movementCoroutine);       
    }

    // Activate this star's linerenderers and change their start and end colors accordingly
    public void DrawConnections()
    {
        for (int i = 0; i < myData.neighbourStars.Count; i++)
        {
            Vector3 startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z + connectionOffset);
            lineRenderers[i].SetPosition(0, startPosition);

            Vector3 tmp = myData.neighbourStars[i].GetComponent<Star>().myData.position;
            Vector3 endPosition = new Vector3(tmp.x, tmp.y, tmp.z + connectionOffset);
            lineRenderers[i].SetPosition(1, endPosition);
          
            lineRenderers[i].startColor = myData.color;

            Color endColor = myData.neighbourStars[i].GetComponent<Star>().myData.color;
            lineRenderers[i].endColor = endColor;

            UseLineRenderer(i + 1, true);
        }
        SetPortal(true);
    }

    // Set portal active or disable it
    private void SetPortal(bool value)
    {
        if (value == false)
        {
            portal.SetActive(false);
            return;
        }

        portal.SetActive(true);
        var particleSystem = portal.GetComponent<ParticleSystem>();
        particleSystem.Play(true);
    }

    // Set one or both line renderers on or off
    public void UseLineRenderer(int index, bool value)
    {
        switch (index)
        {
            case 0:
                lineRendOne.enabled = value;
                lineRendTwo.enabled = value;
                break;

            case 1:
                lineRendOne.enabled = value;
                break;

            case 2:
                lineRendTwo.enabled = value;
                break;

            default:
                break;
        }              
    }

    // Move star to any position in certain duration
    IEnumerator MoveToStarPosition(float duration)
    {
        UseLineRenderer(0, false);

        float counter = 0;
        Vector3 startPos = transform.position;
        Vector3 target = myData.position;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            transform.position = Vector3.Lerp(startPos, target, counter / duration);
            yield return null;
        }

        // If this star is the main focus of the camera, start coroutine to lose focus in certain time
        if (gameObject == GameManager.starManager.GetComponent<StarManager>().GetFocusStar())
        {
            StartCoroutine(CameraFocusHandler());
        }

        transform.position = target;

        // Tell star manager that my star is ready to draw lines
        GameManager.starManager.GetComponent<StarManager>().ReadyToDraw();
    }

    // This coroutine handles the star lifespan and self destruction
    public IEnumerator LifespanHandler(float lifespan)
    {       
        float lifetime = 60 * lifespan;
        yield return new WaitForSeconds(lifetime);
        GameManager.starManager.GetComponent<StarManager>().SelfDestructStar(myData.id);
    }

    // If this star is the main focus of the camera still after 5 seconds, change focus back to star pattern overview
    IEnumerator CameraFocusHandler()
    {
        yield return new WaitForSeconds(5f);

        if (gameObject == GameManager.starManager.GetComponent<StarManager>().GetFocusStar())
        {
            GameManager.mainCamera.GetComponent<MainCamera>().SetCameraState(MainCamera.CameraState.starOverview);
        }
    }

    // Spawn animation coroutine
    IEnumerator SpawnAnimation()
    {
        // Start random spawn animation for star and text
        animator.SetInteger("SpawnAnimationID", Random.Range(0, 8));
        nameText.gameObject.SetActive(true);
        nameText.GetComponent<Animator>().SetInteger("SpawnAnimationID", Random.Range(0, 5));
        
        // Wait until spawn animation is over
        while (!animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            yield return null;
        }

        // Stop fireworks and move to star position
        fireworkEffect.SetInt("SpawnRate", 0);
        GameManager.starManager.GetComponent<StarManager>().RemoveFromSpawningStars(gameObject);
        MoveToStarPosition();
    }

    // Leave animation coroutine
    IEnumerator LeaveAnimation(bool selfDestruct)
    {
        // If this is not self destruction, wait until camera is closer
        if (!selfDestruct)
        {
            while (GameManager.mainCamera.GetComponent<MainCamera>().GetMovingToTarget())
            {
                yield return null;
            }

            yield return new WaitForSeconds(1f);
        }

        // Disable fireworks and linerenderers, destroy the junk storage
        fireworkEffect.SetInt("SpawnRate", 0);
        nameText.GetComponent<Animator>().SetInteger("LeaveAnimationID", Random.Range(0, 5));
        GameManager.starManager.GetComponent<StarManager>().DisableLineRenderers();
        UseLineRenderer(0, false);

        if (myJunkStorage != null)
        {
            myJunkStorage.GetComponent<JunkStorage>().DestroyStorage();
        }

        // Start leave animation and wait until it is over
        animator.SetInteger("LeaveAnimationID", Random.Range(0, 6));
        while (!animator.GetCurrentAnimatorStateInfo(0).IsName("Left"))
        {
            yield return null;
        }

        // Just in case that leave animation doesn't trigger twice
        animator.SetInteger("LeaveAnimationID", -1);

        // If this was self destruct, remove from list of self destructing stars
        if (selfDestruct)
        {
            GameManager.starManager.GetComponent<StarManager>().RemoveFromSelfDestructingStars(gameObject);
        }

        // If this star was the main focus of the camera, double check spawning stars
        if (gameObject == GameManager.starManager.GetComponent<StarManager>().GetFocusStar())
        {
            GameManager.starManager.GetComponent<StarManager>().MoveStarsToPositions();
            GameManager.starManager.GetComponent<StarManager>().CheckSpawningStars();
        }

        // Finally destroy this gameObject
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }
}
