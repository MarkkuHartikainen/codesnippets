﻿using System.Collections.Generic;
using UnityEngine;

// Manager of all star patterns

public class StarPatternManager : MonoBehaviour
{
    // The current star pattern of the scene
    [HideInInspector]
    public StarPattern currentPattern;

    // At start, set default star pattern
    void Awake()
    {
        currentPattern = transform.GetChild(0).GetComponent<StarPattern>();
    }

    // When given list of stars, this function distributes star positions for all of them
    public void SetStarPositions(List<GameObject> listOfStars)
    {
        // Check that the list of stars isn't empty
        if (listOfStars.Count == 0)
        {
            return;
        }
        
        // Choose the star pattern according to the amount of stars
        currentPattern = transform.GetChild(listOfStars.Count - 1).GetComponent<StarPattern>();

        // Distributes star -gameObjects for each star position
        for (int i = 0; i < currentPattern.starPositions.Count; i++)
        {
            currentPattern.starPositions[i].SetStar(listOfStars[i]);          
        }

        // Sets neighbor star -gameObjects for each star position
        for (int i = 0; i < currentPattern.starPositions.Count; i++)
        {
            List<GameObject> listOfNeighbours = new List<GameObject>();

            for (int j = 0; j < currentPattern.starPositions[i].starConnections.Count; j++)
            {
                int newID = currentPattern.starPositions[i].starConnections[j].GetComponent<StarPosition>().id;
                              
                for (int k = 0; k < currentPattern.starPositions.Count; k++)
                {
                    if (newID == currentPattern.starPositions[k].id)
                    {
                        listOfNeighbours.Add(currentPattern.starPositions[k].starObject);
                    }
                }
            }
            currentPattern.starPositions[i].SetNeighbours(listOfNeighbours);
        }

        // Distributes star positions and neihbors for each star in the list
        for (int i = 0; i < listOfStars.Count; i++)
        {
            Vector3 starPos = currentPattern.starPositions[i].position;
            List<GameObject> neighbours = currentPattern.starPositions[i].neighbourStars;
            listOfStars[i].GetComponent<Star>().myData.SetStarPosition(starPos, neighbours);
        }
    }

    // Return current star position
    public StarPattern GetCurrentStarPattern()
    {
        return currentPattern;
    }
}
