﻿using System.Collections.Generic;
using UnityEngine;

// Class for individual star data
public class StarData
{
    // StarData contains info about star id, name, position, color, star-gameObject and list of star's neighbours aka. where to draw star pattern lines
    public int id;
    public string name;
    public Vector3 position;
    public Color color;
    public GameObject starObject;
    public List<GameObject> neighbourStars;

    // Create new StarData and add it on the list of starData
    public StarData(int myID, string myName, GameObject myObject, Color myColor)
    {
        id = myID;
        name = myName;
        starObject = myObject;
        color = myColor;
        StarManager.listOfStars.Add(starObject);
        StarManager.listOfStarData.Add(this);
    }

    // Create new StarData and add it on the list of starData without color
    public StarData(int myID, string myName, GameObject myObject)
    {
        id = myID;
        name = myName;
        starObject = myObject;
        StarManager.listOfStars.Add(starObject);
        StarManager.listOfStarData.Add(this);
    }

    // Set star position and neighbors of this data
    public void SetStarPosition(Vector3 newPosition, List<GameObject> newNeighbours)
    {
        position = newPosition;
        neighbourStars = newNeighbours;
    }
}
