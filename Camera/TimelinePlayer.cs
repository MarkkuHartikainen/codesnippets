﻿using UnityEngine;
using UnityEngine.Playables;

// Timeline aka. cutscene player

public class TimelinePlayer : MonoBehaviour
{
    // Director of this cutscene 
    private PlayableDirector director;

    public GameObject controlPanel;

    private void Awake()
    {
        director = GetComponent<PlayableDirector>();
        director.played += Director_Played;
        director.stopped += Director_Stopped;
    }

    private void Director_Stopped(PlayableDirector obj)
    {
        controlPanel.SetActive(true);
    }

    private void Director_Played(PlayableDirector obj)
    {
        controlPanel.SetActive(false);
    }

    // Start going through timeline aka. cutscene
    public void StartTimeline()
    {
        director.Play();       
    }
}