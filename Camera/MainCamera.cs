﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script for main camera behaviors

[RequireComponent(typeof(Camera))]
public class MainCamera : MonoBehaviour
{
    // Different camera states: camera behavior is based on these
    public enum CameraState { starOverview, starSpawn, followTarget, zoomIn, metroArrival, planetCam};

    // Different offset states of the camera: should camera move inwards, backwards or stay still
    enum OffsetState { offsetInwards, offsetBackwards, offsetReady};

    // The current state of the camera
    CameraState myCameraState = CameraState.starOverview;

    // The last state of the camera
    CameraState lastCameraState = CameraState.starOverview;
    
     // The current camera offset state
     OffsetState myOffsetState; 

    // Current camera offset from the target position
    private Vector3 offset = new Vector3(0f, 0f, -15f);

    // Preferred camera offset from target position, defined at the start
    private Vector3 startOffset;

    // Preferred field of view of the camera
    private float startFov;

    // Extra room around the target bounds, to make sure that the target is fully in frame
    public float overviewMargin = 10f;

    // Extra room around the target bounds during the star spawning
    public float spawnMargin = 4f;

    // Current target position to shoot
    private Vector3 currentPosTarget;

    // Smooths the camera movement
    private float smoothTime = 1.2f;

    // Current camera velocity
    private Vector3 velocity;

    // Minimum zoom distance
    private float minZoom = 40f;

    // Maximum zoom distance
    private float maxZoom = 10f;

    // Limits the zoom amount
    private float xZoomLimit = 0f;

    // Affects the zoom speed
    private float zoomLimiter = 50f;

    // Affects the offset movement speed
    private float offsetLimiter = 2f;

    // Affects the rotation speed of the camera
    private float lookAtSpeed = 0.6f;

    // When metro arrives, camera looks to this target. Default target is metro's face.
    private Transform metroTarget;

    // This camera component
    private Camera cam;

    // Star spawn animation starts atleast after this time, Whether the camera is in optimal shooting position or not.
    private float spawnTimer = 3f;

    // Invisible square around all the spawned stars
    private Bounds bounds;

    // The default position of the camera when it shoots all the stars. Changes according to the star amount.
    private Vector3 starOverviewPos = new Vector3(0f, 0f, -24f);

    // Current Transform target to shoot
    private Transform currentLookTarget;

    // Current Vector3 target to shoot
    private Vector3 currentLookVector;

    // Is camera moving to target position
    private bool movingToTarget = false;

    // Timer between planet camera shots, resets and starts counting when camera is switched on star overview state
    private float planetCamInterval = 10f;

    // Is camera moving back from planet camera shot
    private bool returningFromPlanetCam = false;

    // Saves default camera offset and camera FOV
    void Start()
    {
        cam = GetComponent<Camera>();
        startFov = (int)GetComponent<Camera>().fieldOfView;
        startOffset = offset;

        // Start with the star overview state and look at the junk manager position
        SetCameraState(CameraState.starOverview);
        currentLookTarget = GameManager.junkManager.transform;
        StopCutscene();
    }

    void Update()
    {
        // Shoots all the stars in the star formation
        if (myCameraState == CameraState.starOverview && myOffsetState != OffsetState.offsetReady)
        {
            ZoomOut();
            OffsetHandler();
            LookAtRotation(currentLookVector);
            Move();
        }
        // Shoots specific target with default offset
        else if (myCameraState == CameraState.zoomIn)
        {
            ZoomIn();
            LookAtRotation();
            Move();
        }
        // Shoots just spawned stars
        else if (myCameraState == CameraState.starSpawn)
        {
            OffsetHandlerSpawn();

            // Trigger star spawning animation, if camera is close to the spawning star or if spawnTimer is zero
            if (Vector3.Distance(transform.position, currentPosTarget + startOffset) < 5 || spawnTimer <= 0f)
            {
                GameManager.starManager.GetComponent<StarManager>().TriggerSpawnAnimation();
                LookAtRotation(currentLookVector);
                ZoomIn();
            }
            // If star is spawning when camera is shooting metro arrival or returning from planet cam, zoom out and use different movement speed, to make movement more natural
            else if (lastCameraState == CameraState.metroArrival || returningFromPlanetCam)
            {
                SetCameraValues(0.5f, offsetLimiter, 2f);
                LookAtRotation(currentLookVector);
                ZoomOut();
                spawnTimer -= Time.deltaTime;
            }
            // Zoom in while moving closer to the spawning star
            else
            {
                ZoomIn();
                spawnTimer -= Time.deltaTime;               
            }
            Move();
        }
        // Follow and shoot current target
        else if (myCameraState == CameraState.followTarget)
        {
            currentPosTarget = currentLookTarget.position;
            LookAtRotation();
            Move();
        }
        // When metro arrives, look at the metroTarget aka. metro's face
        else if (myCameraState == CameraState.metroArrival)
        {
            currentLookTarget = metroTarget.transform;
            currentPosTarget = starOverviewPos;
            LookAtRotation();
            Move();
        }     
    }

    // Rotate camera towards target transform, with specific lookAtSpeed
    void LookAtRotation()
    {
        Vector3 relativePos = currentLookTarget.position - transform.position;
        Quaternion toRotation = Quaternion.LookRotation(relativePos);
        transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, lookAtSpeed * Time.deltaTime);
    }

    // Rotate camera towars target Vector3, with specific lookAtSpeed
    void LookAtRotation(Vector3 target)
    {
        Vector3 relativePos = target - transform.position;
        Quaternion toRotation = Quaternion.LookRotation(relativePos);
        transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, lookAtSpeed * Time.deltaTime);
    }

    // Moves camera smoothly to the currentTarget + offset
    void Move()
    {
        Vector3 newPosition = currentPosTarget + offset;
        transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime);

        // If distance to target is more than 2f, camera is moving to the target. This information is used elsewhere
        if (Vector3.Distance(transform.position, newPosition) > 2f)
        {
            movingToTarget = true;
        }
        else
        {
            movingToTarget = false;
            returningFromPlanetCam = false;
        }
    }

    // Zooms in until xZoomLimit is reached. zoomLimiter affects the zoom speed
    void ZoomIn()
    {
        float newZoom = Mathf.Lerp(maxZoom, minZoom, xZoomLimit / zoomLimiter);
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, newZoom, Time.deltaTime);
    }

    // Zooms out to the preferred starting camera FOV
    void ZoomOut()
    {
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, startFov, 0.3f * Time.deltaTime);
    }

    // Moves the camera backwards and/or inwards during star overview state until all stars fit nicely in the camera frame
    public void OffsetHandler()
    {
        // If right offset is achieved, return
        if (myOffsetState == OffsetState.offsetReady || movingToTarget == true)
        {           
            return;
        }

        // Correct camera distance from the stars is calculated by drawing rectangle around the spawned stars. OverviewMargin is added to the corners of the rectangle to make every star fit nicely in the frame.

        // Upper left corner pos of the target frame
        Vector3 leftCorner = new Vector3 (bounds.center.x - (bounds.size.x / 2) + overviewMargin, bounds.center.y - (bounds.size.y / 2) + overviewMargin, bounds.center.z);
        // Lower right corner pos of the target frame
        Vector3 rightCorner = new Vector3(bounds.center.x + (bounds.size.x / 2) + overviewMargin, bounds.center.y + (bounds.size.y / 2) + overviewMargin, bounds.center.z);

        // Move camera backwards until target frame is seen by the camera
        if (myOffsetState == OffsetState.offsetBackwards)
        {
            if (!IsSeenByCamera(leftCorner) || !IsSeenByCamera(rightCorner))
            {
                offset.z -= offsetLimiter * Time.deltaTime;
            }
            else
            {
                myOffsetState = OffsetState.offsetInwards;
            }
        }

        // When target frame is seen by the camera, move camera inwards until the target frame is no more seen
        if (myOffsetState == OffsetState.offsetInwards)
        {
            if (IsSeenByCamera(leftCorner) && IsSeenByCamera(rightCorner))
            {
                offset.z += offsetLimiter * Time.deltaTime;
            }
            else
            {
                myOffsetState = OffsetState.offsetReady;
                starOverviewPos = transform.position;
                // After moving backwars and inwards, camera is finally in it's newly defined starOverviesPos. Now it's time to inform the GameManager about this new position
                GameManager.UpdateCameraPosition(starOverviewPos);
            }
        }
    }

    // Moves the camera backwards and/or inwards during the star spawning state until all currently spawning stars fit nicely in the camera frame
    public void OffsetHandlerSpawn()
    {
        // Correct camera distance from the currently spawning stars is calculated by drawing rectangle around the spawning stars. SpawnMargins is added to the corners of the rectangle to make every star fit nicely in the frame.

        // Upper left corner pos of the target frame
        Vector3 leftCorner = new Vector3(bounds.center.x - (bounds.size.x / 2) + spawnMargin, bounds.center.y - (bounds.size.y / 2) + spawnMargin, bounds.center.z);
        // Lower right corner pos of the target frame
        Vector3 rightCorner = new Vector3(bounds.center.x + (bounds.size.x / 2) + spawnMargin, bounds.center.y + (bounds.size.y / 2) + spawnMargin, bounds.center.z);

        // Move camera backwards until target frame is seen by the camera
        if (myOffsetState == OffsetState.offsetBackwards)
        {
            if (!IsSeenByCamera(leftCorner) || !IsSeenByCamera(rightCorner))
            {
                offset.z -= offsetLimiter * Time.deltaTime;
            }
            else
            {
                myOffsetState = OffsetState.offsetInwards;
            }
        }

        // When target frame is seen by the camera, move camera inwards until the target frame is no more seen
        if (myOffsetState == OffsetState.offsetInwards)
        {
            if (IsSeenByCamera(leftCorner) && IsSeenByCamera(rightCorner))
            {
                offset.z += offsetLimiter * Time.deltaTime;
            }
        }
    }

    // Checks if a specific target is inside the camera view
    bool IsSeenByCamera(Vector3 target)
    {
        Vector3 screenPoint = GetComponent<Camera>().WorldToViewportPoint(target);
        if (screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1)
        {
            return true;
        }
        return false;
    }

    // Sets new target to zoom at
    public void ZoomInToPos(Transform pos)
    {
        currentPosTarget = pos.position;
        currentLookTarget = pos;
    }

    // Returns camera's distance from the current target
    public float GetDistanceFromTarget()
    {
        return Vector3.Distance(transform.position, currentPosTarget);
    }

    // When one or more stars spawn, this calculates the center point of these stars. Camera can now shoot this center point
    public void SetSpawnCenter(List<GameObject> spawningStars)
    {
        if(spawningStars.Count <= 0) { return; }
        
        var totalX = 0f;
        var totalY = 0f;
        var totalZ = 0f;

        foreach (var star in spawningStars)
        {
            totalX += star.transform.position.x;
            totalY += star.transform.position.y;
            totalZ += star.transform.position.z;
        }
        var centerX = totalX / spawningStars.Count;
        var centerY = totalY / spawningStars.Count;
        var centerZ = totalZ / spawningStars.Count;

        Vector3 spawnCenter = new Vector3(centerX, centerY, centerZ);

        bounds = new Bounds(spawnCenter, Vector3.zero);
        for (int i = 0; i < spawningStars.Count; i++)
        {
            bounds.Encapsulate(spawningStars[i].transform.position);
        }

        // xZoomLimits prevents camera from zooming too close to the spawning stars
        xZoomLimit = 15 * spawningStars.Count;

        // Current position and looking targets are set for this newly calculated star spawning center
        currentLookVector = bounds.center;
        currentPosTarget = bounds.center;
    }

    // Set camera state to something
    public void SetCameraState(CameraState newState)
    {
        // Saves the last camera state
        lastCameraState = myCameraState;

        // If camera is shooting planets when new state is called, disable planet cam
        if (myCameraState == CameraState.planetCam)
        {
            GameManager.solarSystem.GetComponent<SolarSystem>().SetPlanetCam(false);         
            
            // If new camera state is starOverview, fly smoothly to star overview pos from planet cam position
            if (newState == CameraState.starOverview)
            {
                returningFromPlanetCam = true;
                myCameraState = newState;
                myOffsetState = OffsetState.offsetBackwards;
                SetCameraValues(4f, offsetLimiter, 1f);
                StartCoroutine(PlanetCamManager());
                return;
            }

            StopCutscene();
            myCameraState = CameraState.starOverview;
        }

        // Set different camera speed, rotation, offset etc. values according to the new camera state
        switch (newState)
        {
            case CameraState.starOverview:

                // Get bounds of the stars from starManager
                bounds = GameManager.starManager.GetComponent<StarManager>().GetBounds();

                // When changing to the starOverview state, take correct camera speed, rotation and offset values right from the current star pattern. Because star patterns differ from scale, camera movement values must take that in account
                SetCameraValues(GameManager.starPatternManager.GetComponent<StarPatternManager>().GetCurrentStarPattern().GetSmoothAmount(),
                    GameManager.starPatternManager.GetComponent<StarPatternManager>().GetCurrentStarPattern().GetBackwardSpeed(),
                    GameManager.starPatternManager.GetComponent<StarPatternManager>().GetCurrentStarPattern().GetRotationSpeed());

                myOffsetState = OffsetState.offsetBackwards;
                currentPosTarget = bounds.center;
                currentLookVector = bounds.center;
                
                // Start timer for next planet cam shot
                StartCoroutine(PlanetCamManager());
                break;

            case CameraState.starSpawn:
                offset = startOffset;
                SetCameraValues(1.2f, 2f, 0.6f);
                spawnTimer = 3f;
                myOffsetState = OffsetState.offsetBackwards;
                StopAllCoroutines();
                break;

            case CameraState.followTarget:
                offset = startOffset;
                SetCameraValues(smoothTime, offsetLimiter, 1.5f);
                StopAllCoroutines();
                break;

            case CameraState.metroArrival:
                // When metro arrives, ignore it if camera is not in starOverview state
                if (myCameraState != CameraState.starOverview)
                {
                    return;
                }
                offset = startOffset;
                currentLookTarget = metroTarget.transform;
                currentPosTarget = starOverviewPos;
                SetCameraValues(4f, offsetLimiter, 1.6f);
                StopAllCoroutines();
                break;

            case CameraState.zoomIn:
                movingToTarget = true;
                xZoomLimit = 10;
                SetCameraValues(0.6f, offsetLimiter, lookAtSpeed);
                StopAllCoroutines();
                break;
            default:
                break;
        }
        myCameraState = newState;
    }

    // Function to set new camera movement speed, offset movement speed, and rotation speed
    private void SetCameraValues(float newSmoothTime, float newOffsetLimiter, float newLookAtSpeed)
    {
        smoothTime = newSmoothTime;
        offsetLimiter = newOffsetLimiter;
        lookAtSpeed = newLookAtSpeed;
    }

    // Start followTarget camera state and set the transform to follow
    public void FollowTarget(Transform newTarget)
    {
        currentLookTarget = newTarget;
        SetCameraState(CameraState.followTarget);
    }

    // Set metroTarget aka. the Transform that the camera shoots when metro arrives
    public void SetMetroTarget(Transform target)
    {
        metroTarget = target;
    }

    // Set new field of view
    public void SetFOV(float value)
    {
        if (startFov != value)
        {
            startFov = value;
            myOffsetState = OffsetState.offsetBackwards;
            GetComponent<Camera>().fieldOfView = value;
        }       
    }

    // Update the bounds of the stars aka. invisible rectangle around the stars
    public void UpdateBounds(Bounds newBounds)
    {
        bounds = newBounds;
    }

    // Returns current starOverviesPos
    public Vector3 GetStarOverviewPos()
    {
        return starOverviewPos;
    }

    // Returns whether the camera is currently moving to target
    public bool GetMovingToTarget()
    {
        return movingToTarget;
    }

    // When cutscene ends, camera spawns to current starOverviesPos and looks forward
    public void StopCutscene()
    {
        transform.position = starOverviewPos;
        transform.localRotation = Quaternion.LookRotation(Vector3.forward);
    }

    // Return current camera state
    public CameraState GetCurrentCameraState()
    {
        return myCameraState;
    }

    // Set interval between planet cam shots
    public void SetPlanetCamInterval(float value)
    {
        planetCamInterval = value;
        StopAllCoroutines();
        if (myCameraState == CameraState.starOverview)
        {
            StartCoroutine(PlanetCamManager());
        }
        else if (myCameraState == CameraState.planetCam)
        {
            SetCameraState(CameraState.starOverview);
        }
    }

    // Coroutine that handles the planet cam shots
    IEnumerator PlanetCamManager()
    {
        // Wait the planet cam interval time before shooting a planet
        float waitTimer = planetCamInterval;
        while (waitTimer > 0)
        {
            waitTimer -= Time.deltaTime;
            yield return null;
        }

        // When wait is over, check if camera is doing something more important
        if (myCameraState != CameraState.starOverview)
        {
            yield break;
        }

        // If not, now it is time for planet cam shot! Activate a planet cam! 
        SetCameraState(CameraState.planetCam);
        GameManager.solarSystem.GetComponent<SolarSystem>().SetPlanetCam(true);

        // This is the duration of the shot. Something between 6 to 15 seconds.
        float showTimer = Random.Range(6f, 15f);
        while (showTimer > 0)
        {
            showTimer -= Time.deltaTime;
            yield return null;
        }

        // When shot is over, return camera to starOverview state
        SetCameraState(CameraState.starOverview);
    }
}
