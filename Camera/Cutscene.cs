﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

// This script plays and stops a cutscene when metro arrives

public class Cutscene : MonoBehaviour
{
    // List of this cutscene's cameras
    public List<GameObject> myCams;

    // Director component of this cutscene
    private PlayableDirector director;

    // Find the director component
    void Start()
    {
        director = GetComponent<PlayableDirector>();
    }

    // Check whether this cutscene is playing
    public bool IsPlaying()
    {
        StopAllCoroutines();
        return director.state == PlayState.Playing;
    }

    // Disable cameras and stop the cutscene
    public void StopCutscene()
    {
        director.Stop();
        foreach (GameObject cam in myCams)
        {
            cam.SetActive(false);
        }
    }

    // Set first camera active and start the cutscene coroutine
    public void PlayCutscene()
    {
        myCams[0].SetActive(true);
        StopAllCoroutines();
        StartCoroutine(Play());
    }

    // Coroutine for playing the cutscene
    IEnumerator Play()
    {
        yield return null;

        director.Play();

        // Set each camera active
        for (int i = 1; i < myCams.Count; i++)
        {
            myCams[i].SetActive(true);
        }

        // While cutscene is playing wait
        while (director.state == PlayState.Playing)
        {
            yield return null;
        }

        // When cutscene has ended, disable cameras
        foreach (GameObject cam in myCams)
        {
            cam.SetActive(false);
        }

        // Inform the cutscene manager that this cutscene has ended
        GameManager.cutsceneManager.GetComponent<CutsceneManager>().CutsceneEnded();

        // Inform the main camera that this cutscene has ended
        GameManager.mainCamera.GetComponent<MainCamera>().StopCutscene();

        // Set camerastate to star overview
        GameManager.mainCamera.GetComponent<MainCamera>().SetCameraState(MainCamera.CameraState.starOverview);
    }
}
