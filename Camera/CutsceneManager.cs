﻿using UnityEngine;


// Manager of all metro cutscenes

public class CutsceneManager : MonoBehaviour
{
    // Bool whether any of the cutscenes is playing
    private bool isPlaying = false;

    // Stop currently playing cutscene
    public void StopPlaying()
    {
        foreach (Transform child in transform)
        {
            if (child.GetComponent<Cutscene>().IsPlaying())
            {
                child.GetComponent<Cutscene>().StopCutscene();
                GameManager.mainCamera.GetComponent<MainCamera>().StopCutscene();
                GameManager.metroRoute.GetComponent<MetroRoute>().ResetAnimation();
                CutsceneEnded();
            }
        }
    }

    // All cutscenes have stopped playing
    public void CutsceneEnded()
    {
        isPlaying = false;
    }

    // If there are no cutscenes playing, play a random cutscene
    public void PlayRandomCutscene()
    {
        if (isPlaying == false)
        {
            isPlaying = true;
            int randomNum = Random.Range(0, transform.childCount);
            transform.GetChild(randomNum).gameObject.GetComponent<Cutscene>().PlayCutscene();
        }       
    }

    // Return info whether any cutscenes are playing
    public bool GetIsPlaying()
    {
        return isPlaying;
    }
}
