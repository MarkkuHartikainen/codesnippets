﻿using UnityEngine;

// This script makes gameObject to face towards target gameObject

public class LookAtObject : MonoBehaviour
{
    // The target gameObject
    public GameObject target = null;

    // If there is no specific target, face towards main camera
    void Start()
    {
        if (target == null)
        {
            target = GameManager.mainCamera;
        }       
    }

    // Look at the target -gameObject
    void Update()
    {
        transform.LookAt(target.transform.position, Vector3.right);
        transform.rotation = Quaternion.LookRotation(transform.position - target.transform.position);
    }
}
