﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using UnityEngine.Rendering;

// Manager of all settings in the pause menu

public class SettingsManager : MonoBehaviour
{
    // PauseMenu -GameObject aka. canvas
    public GameObject pauseMenu;

    // VideoMenu -GameObject aka. canvas
    public GameObject videoMenu;

    // GameplayMenu -GameObject aka. canvas
    public GameObject gameplayMenu;

    // LogOutMenu -GameObject aka. canvas
    public GameObject logOutMenu;

    // PostProcessVolume of this game
    private VolumeProfile volumeProfile;

    // Resolution Inputfield -GameObject and it's setting
    public TMP_InputField resolutionBox;
    Setting resolutionSet;

    // FullScreen Inputfield -GameObject and it's setting
    public TMP_InputField fullscreenBox;
    Setting fullScreenSet;

    // Camera field of view Inputfield -GameObject and it's setting
    public TMP_InputField fovBox;
    Setting fovSet;

    // Quality Inputfield -GameObject and it's setting
    public TMP_InputField qualityBox;
    Setting qualitySet;

    // Bloom amount Inputfield and it's setting
    public TMP_InputField bloomBox;
    Setting bloomSet;

    // Saturation amount Inputfield and it's setting
    public TMP_InputField saturationBox;
    Setting saturationSet;

    // Gamma amount Inputfield and it's setting
    public TMP_InputField gammaBox;
    Setting gammaSet;

    // Star max amount Inputfield and it's setting
    public TMP_InputField starMaxAmountBox;
    Setting starMaxAmountSet;

    // Star lifespan Inputfield and it's setting
    public TMP_InputField starLifespanBox;
    Setting starLifespanSet;

    // Allow fake stars Inputfield and it's setting
    public TMP_InputField allowFakeStarsBox;
    Setting allowFakeStarsSet;

    // Metro interval Inputfield and it's setting
    public TMP_InputField metroIntervalBox;
    Setting metroIntervalSet;

    // Planet camera interval Inputfield and it's setting
    public TMP_InputField planetCamIntervalBox;
    Setting planetCamIntervalSet;

    // Junk distance Inputfield and it's setting
    public TMP_InputField junkDistanceBox;
    Setting junkDistanceSet;

    // List of all settings
    public static List<Setting> allSettings = new List<Setting>();

    // List of all video settings
    public static List<Setting> allVideoSettings = new List<Setting>();

    // List of all gameplay settings
    public static List<Setting> allGameplaySettings = new List<Setting>();

    // Video settings apply -button gameObject
    public Button videoApplyButton;

    // Gameplay settings apply -button gameObject
    public Button gameplayApplyButton;

    // Resume -button gameObject
    public Button resumeButton;

    // LogOut -button gameObject
    public Button logOutButton;

    // Panel for scrolling / navigating the pause menu
    public GameObject pauseMenuSelector;

    // Panel for scrolling / navigating the video settings menu
    public GameObject videoMenuSelector;

    // Panel for scrolling / navigating the gameplay settings menu
    public GameObject gameplayMenuSelector;

    // Current menu selector
    private GameObject selector;

    // Id of the setting player is currently editing
    public static int mySelection = 0;

    // Two different categories of the settings
    public enum SettingCategory {video, gameplay};

    // Array of the available resolutions
    Resolution[] resolutions;

    // The direction from where the player brought the pause menu
    private Direction lastDirection;

    // Static boolean whether the pause menu is currently on the screen
    public static bool pauseMenuOn = false;

    // FPS counter of the pause menu
    public GameObject fpsCounter;

    private void Start()
    {
        // Hides pauseMenu
        pauseMenu.transform.position = new Vector2(Screen.width / 2, Screen.height / 2 + Screen.height * 2);

        // Gets curren post processing volume
        volumeProfile = GameManager.globalVolume.GetComponent<Volume>().profile;

        // Retrieves all available screen resolutions
        resolutions = Screen.resolutions;
        if (resolutions.Length > 0)
        {
            List<string> options = new List<string>();

            for (int i = 0; i < resolutions.Length; i++)
            {
                string option = resolutions[i].width + " x " + resolutions[i].height;
                if (options.Contains(option))
                {
                    continue;
                }

                options.Add(option);

                if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
                {
                    resolutionSet = new Setting("Resolution", resolutionBox, i, 0, resolutions.Length - 1, 1, SettingCategory.video);
                }
            }
        }

        CreateSettings();
        StartAdjust();
        RetrieveValues();

        ContinueGame();
    }


    // Creates setting variables and saves them to list allSettings
    private void CreateSettings()
    {
        if (resolutions.Length <= 0)
        {
            resolutionSet = new Setting("Resolution", resolutionBox, 0, 0, 0, 0, SettingCategory.video);
        }     

        fullScreenSet = new Setting("FullScreen", fullscreenBox, 0, 0, 1, 1, SettingCategory.video);
        fovSet = new Setting("CameraFOV", fovBox, 60, 60, 120, 5, SettingCategory.video);
        qualitySet = new Setting("Quality", qualityBox, 0, 0, 3, 1, SettingCategory.video);
        bloomSet = new Setting("Bloom", bloomBox, 0.5f, 0, 2, 0.1f, SettingCategory.video);
        saturationSet = new Setting("Saturation", saturationBox, 10, -100, 100, 10, SettingCategory.video);
        gammaSet = new Setting("Gamma", gammaBox, -0.2f, -1, 1, 0.1f, SettingCategory.video);

        starMaxAmountSet = new Setting("StarMaxAmount", starMaxAmountBox, 15, 5, 110, 5, SettingCategory.gameplay);
        starLifespanSet = new Setting("StarLifespan", starLifespanBox, 15, 1, 21, 1, SettingCategory.gameplay);
        allowFakeStarsSet = new Setting("AllowFakeStars", allowFakeStarsBox, 0, 0, 1, 1, SettingCategory.gameplay);
        metroIntervalSet = new Setting("MetroInterval", metroIntervalBox, 3, 1f, 15, 1f, SettingCategory.gameplay);
        planetCamIntervalSet = new Setting("PlanetCamInterval", planetCamIntervalBox, 90, 15, 300, 15, SettingCategory.gameplay);
        junkDistanceSet = new Setting("JunkDistance", junkDistanceBox, 6, 1, 10, 1, SettingCategory.gameplay);
    }

    // Retrieves already saved settings and applies them at the beginning of the game
    public void StartAdjust()
    {
        SetResolution(resolutionSet.GetSavedValue());
        SetFullscreen(fullScreenSet.GetSavedValue());
        SetCameraFOV(fovSet.GetSavedValue());
        SetQuality(qualitySet.GetSavedValue());
        SetBloom(bloomSet.GetSavedValue());
        SetSaturation(saturationSet.GetSavedValue());
        SetGamma(gammaSet.GetSavedValue());

        SetMaxStarAmount(starMaxAmountSet.GetSavedValue());
        SetStarLifespan(starLifespanSet.GetSavedValue());
        SetAllowFakeStars(allowFakeStarsSet.GetSavedValue());
        SetMetroInterval(metroIntervalSet.GetSavedValue());
        SetPlanetCamInterval(planetCamIntervalSet.GetSavedValue());
        SetJunkDistance(junkDistanceSet.GetSavedValue());       
    }

    // Retrieves saved setting values from memory
    public void RetrieveValues()
    {
        foreach (Setting set in allSettings)
        {
            set.SetCurrentValue(set.GetSavedValue());
        }
        RefreshValues();      
    }

    // Updates current setting values of the pause menu (updates correct texts to inputfields)
    void RefreshValues()
    {
        if (resolutions.Length > 0)
        {
            resolutionBox.text = resolutions[(int)resolutionSet.GetCurrentValue()].ToString();
        }
        
        if (resolutionBox.text.Contains("@"))
        {
            resolutionBox.text = resolutionBox.text.Remove(resolutionBox.text.IndexOf("@"));
        }

        if (fullScreenSet.GetCurrentValue() == 1)
        {
            fullscreenBox.text = "FullScreen";
        }
        else
        {
            fullscreenBox.text = "Windowed";
        }

        fovBox.text = fovSet.GetCurrentValue().ToString();
        switch (qualitySet.GetCurrentValue())
        {
            case 0:
                qualityBox.text = "0x";
                break;
            case 1:
                qualityBox.text = "2x";
                break;
            case 2:
                qualityBox.text = "4x";
                break;
            case 3:
                qualityBox.text = "8x";
                break;
            default:
                break;
        }
        
        bloomBox.text = (bloomSet.GetCurrentValue() * 10).ToString("F0");
        saturationBox.text = saturationSet.GetCurrentValue().ToString();
        gammaBox.text = (gammaSet.GetCurrentValue() * 10).ToString("F0");

        starMaxAmountBox.text = starMaxAmountSet.GetCurrentValue().ToString();

        if (starLifespanSet.GetCurrentValue() > 20) { starLifespanBox.text = "Infinite";}
        else { starLifespanBox.text = starLifespanSet.GetCurrentValue().ToString() + " min"; }

        switch (allowFakeStarsSet.GetCurrentValue())
        {
            case 0:
                allowFakeStarsBox.text = "No";
                break;

            case 1:
                allowFakeStarsBox.text = "Yes";
                break;
            default:
                break;
        }

        metroIntervalBox.text = metroIntervalSet.GetCurrentValue().ToString() + " min";
        planetCamIntervalBox.text = planetCamIntervalSet.GetCurrentValue().ToString() + " s";
        junkDistanceBox.text = junkDistanceSet.GetCurrentValue().ToString() + " m";
    }

    public void Update()
    {
        // Sets active or disables PauseMenu when "ESC" is pressed 
        if (Input.touchCount == 0 && Input.GetKeyDown(KeyCode.Escape) && !GameManager.gameHasEnded)
        {
            PauseMenuAnimation(lastDirection);
            PauseMenu();
        }
    }

    // Starts pauseMenu animation to last used direction
    public void MovePauseMenu()
    {
        PauseMenuAnimation(lastDirection);
    }

    // Sets Resolution to specific value and saves it 
    public void SetResolution(float resolutionIndex)
    {
        if (resolutions.Length > 0)
        {
            Resolution resolution = resolutions[(int)resolutionIndex];
            Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
            RefreshValues();
            resolutionSet.SaveValue();
        }
    }

    // Sets FullScreen to specific value and saves it
    public void SetFullscreen(float number)
    {
        if (number == 0)
        {
            fullscreenBox.text = "Windowed";
            Screen.fullScreen = false;
        }

        if (number == 1)
        {
            fullscreenBox.text = "FullScreen";
            Screen.fullScreen = true;
        }
        fullScreenSet.SaveValue();
    }

    // Sets main camera Field of view to specific value and saves it
    public void SetCameraFOV(float value)
    {
        if (value >= 60)
        {
            GameManager.mainCamera.GetComponent<MainCamera>().SetFOV(value);
            fovBox.text = value.ToString();
            fovSet.SaveValue();
        }
    }

    // Sets Graphics Quality to specific value and saves it
    public void SetQuality(float qualityValue)
    {
        switch (qualityValue)
        {
            case 0:
                QualitySettings.antiAliasing = 0;
                break;
            case 1:
                QualitySettings.antiAliasing = 2;
                break;
            case 2:
                QualitySettings.antiAliasing = 4;
                break;
            case 3:
                QualitySettings.antiAliasing = 8;
                break;
            default:
                break;
        }
        qualitySet.SaveValue();
    }

    // Sets Bloom amount
    public void SetBloom(float value)
    {
        UnityEngine.Rendering.Universal.Bloom bloom;
        if (!volumeProfile.TryGet(out bloom)) throw new System.NullReferenceException(nameof(bloom));
        bloom.intensity.Override(value);    
    }

    // Sets Saturation amount
    public void SetSaturation(float value)
    {
        UnityEngine.Rendering.Universal.ColorAdjustments color;
        if (!volumeProfile.TryGet(out color)) throw new System.NullReferenceException(nameof(color));
        color.saturation.Override(value);
    }

    // Sets Gamma amount
    public void SetGamma(float value)
    {
        Vector4 newGamma = new Vector4(value, value, value, value);
        UnityEngine.Rendering.Universal.LiftGammaGain light;
        if (!volumeProfile.TryGet(out light)) throw new System.NullReferenceException(nameof(light));
        light.gamma.Override(newGamma);
    }

    // Sets max star amount to specific value and saves it
    public void SetMaxStarAmount(float value)
    {
        GameManager.starManager.GetComponent<StarManager>().SetMaxStarAmount(value);
        starMaxAmountSet.SaveValue();
    }

    // Sets star lifespan to specific value and saves it
    public void SetStarLifespan(float value)
    {
        GameManager.starManager.GetComponent<StarManager>().SetStarLifespan(value);
        starLifespanSet.SaveValue();
    }

    // Sets "allow fake stars" to specific value and saves it
    public void SetAllowFakeStars(float value)
    {
        GameManager.SetAllowFakeStars(value);
        allowFakeStarsSet.SaveValue();
    }

    // Sets MetroInterval -time to specific value and saves it
    public void SetMetroInterval(float value)
    {
        float time = 60 * value;
        GameManager.metroRoute.GetComponent<MetroRoute>().SetMetroInterval(time);
        metroIntervalSet.SaveValue();
    }

    // Sets Planet camera interval -time to specific value and saves it
    public void SetPlanetCamInterval(float value)
    {
        GameManager.mainCamera.GetComponent<MainCamera>().SetPlanetCamInterval(value);
        planetCamIntervalSet.SaveValue();
    }

    // Sets JunkDistance -amount to specific value and saves it
    public void SetJunkDistance(float value)
    {
        GameManager.junkManager.GetComponent<JunkManager>().SetJunkDistance(value);
        junkDistanceSet.SaveValue();
    }

    // Left-arrow UI button functionality 
    public void LeftArrowButton(int i)
    {
        mySelection = i;
        MoveSelector(0);
        UseSelector(Direction.Left);
    }

    // Right-arrow UI button functionality 
    public void RightArrowButton(int i)
    {
        mySelection = i;
        MoveSelector(0);
        UseSelector(Direction.Right);
    }

    // Apply video settings
    public void ApplyVideoSettings()
    {
        SetResolution(resolutionSet.GetCurrentValue());
        SetFullscreen(fullScreenSet.GetCurrentValue());
        SetCameraFOV(int.Parse(fovBox.text));
        SetQuality(qualitySet.GetCurrentValue());
        bloomSet.SaveValue();
        saturationSet.SaveValue();
        gammaSet.SaveValue();
        
        videoApplyButton.GetComponent<Button>().interactable = false;
    }

    // Apply gameplay settings
    public void ApplyGameplaySettings()
    {
        SetMaxStarAmount(starMaxAmountSet.GetCurrentValue());
        SetStarLifespan(starLifespanSet.GetCurrentValue());
        SetAllowFakeStars(allowFakeStarsSet.GetCurrentValue());
        SetMetroInterval(metroIntervalSet.GetCurrentValue());
        SetPlanetCamInterval(planetCamIntervalSet.GetCurrentValue());
        SetJunkDistance(junkDistanceSet.GetCurrentValue());

        gameplayApplyButton.GetComponent<Button>().interactable = false;
    }

    // Opens pausemenu, hides rest
    public void PauseMenu()
    {
        selector = pauseMenuSelector;
        mySelection = 0;
        MoveSelectorPauseMenu();
        RetrieveValues();

        SetBloom(bloomSet.GetSavedValue());
        SetSaturation(saturationSet.GetSavedValue());
        SetGamma(gammaSet.GetSavedValue());

        gameplayMenu.SetActive(false);
        logOutMenu.SetActive(false);
        videoMenu.SetActive(false);
        pauseMenu.SetActive(true);
        fpsCounter.GetComponent<FPSCounter>().enabled = true;
    }

    // Opens video settings menu, hides rest
    public void VideoSettings()
    {       
        selector = videoMenuSelector;
        mySelection = 0;
        MoveSelectorVideoMenu();
        CheckApplyButtonState(videoApplyButton, allVideoSettings);

        pauseMenu.SetActive(false);
        gameplayMenu.SetActive(false);
        logOutMenu.SetActive(false);
        videoMenu.SetActive(true);
    }

    // Opens gameplay settings menu, hides rest
    public void GameplaySettings()
    {
        selector = gameplayMenuSelector;
        mySelection = 0;
        MoveSelectorGameplayMenu();
        CheckApplyButtonState(gameplayApplyButton, allGameplaySettings);

        pauseMenu.SetActive(false);
        videoMenu.SetActive(false);
        logOutMenu.SetActive(false);
        gameplayMenu.SetActive(true);
    }

    // Hides all open menus
    public void ContinueGame()
    {
        videoMenu.SetActive(false);
        gameplayMenu.SetActive(false);
        logOutMenu.SetActive(false);
        fpsCounter.GetComponent<FPSCounter>().enabled = false;
    }

    // LogOut -button functionality
    public void LogOutButton()
    {
        GameManager.LogOut();
        pauseMenu.SetActive(false);
        logOutMenu.SetActive(true);       
    }

    // Quit functionality
    public void QuitGame()
    {
        print("Quit Game");
        Application.Quit();
    }

    // Moves selection panel / navigator up and down according to the open menu
    public void MoveSelector(int increment)
    {
        if (selector == pauseMenuSelector)
        {
            mySelection = Mathf.Clamp(mySelection + increment, 0, 3);
            MoveSelectorPauseMenu();
        }
        else if (selector == videoMenuSelector)
        {
            mySelection = Mathf.Clamp(mySelection + increment, 0, 8);
            MoveSelectorVideoMenu();
        }
        else if (selector == gameplayMenuSelector)
        {
            mySelection = Mathf.Clamp(mySelection + increment, 0, 7);
            MoveSelectorGameplayMenu();
        }
    }

    // Moves selection panel / navigator up and down the pause menu
    private void MoveSelectorPauseMenu()
    {
        Vector2 target = selector.transform.localPosition;
        switch (mySelection)
        {
            case 0:
                target.y = 175f;
                break;
            case 1:
                target.y = 72f;
                break;
            case 2:
                target.y = -110f;
                break;
            case 3:
                target.y = -197f;
                break;
            default:
                break;
        }
        StartCoroutine(MoveSelectorToPos(selector, target, 0.2f));
    }

    // Moves selection panel / navigator up and down the video settings menu
    public void MoveSelectorVideoMenu()
    { 
        Vector2 target = selector.transform.localPosition;
     
        if (mySelection <= 6)
        {
            target.y = allVideoSettings[mySelection].GetBoxY();
        }
        else
        {
            switch (mySelection)
            {
                case 7:
                    target.y = videoApplyButton.transform.localPosition.y;
                    break;
                case 8:
                    target.y = -390f;
                    break;
                default:
                    break;
            }
        }
        StartCoroutine(MoveSelectorToPos(selector, target, 0.2f));
    }

    // Moves selection panel / navigator up and down the gameplay settings menu
    public void MoveSelectorGameplayMenu()
    {
        Vector2 target = selector.transform.localPosition;

        if (mySelection <= 5)
        {
            target.y = allGameplaySettings[mySelection].GetBoxY();
        }
        else
        {
            switch (mySelection)
            {
                case 6:
                    target.y = gameplayApplyButton.transform.localPosition.y;
                    break;
                case 7:
                    target.y = -350f;
                    break;
                default:
                    break;
            }
        }
        StartCoroutine(MoveSelectorToPos(selector, target, 0.2f));
    }

    // Use currently selected menu item (mySelection)
    public void UseSelector(Direction dir)
    {
        if (selector == pauseMenuSelector)
        {
            switch (mySelection)
            {
                case 0:
                    VideoSettings();
                    break;
                case 1:
                    GameplaySettings();
                    break;
                case 2:
                    ContinueGame();
                    PauseMenuAnimation(lastDirection);
                    break;
                case 3:
                    LogOutButton();
                    break;
                default:
                    break;
            }
        }
        else if (selector == videoMenuSelector)
        {
            // Increase or decrease the video setting value that is currently selected
            if (mySelection <= 6)
            {
                if (dir == Direction.Right) { allVideoSettings[mySelection].Increase(); }
                else if (dir == Direction.Left) { allVideoSettings[mySelection].Decrease(); }

                switch (mySelection)
                {
                    case 4:
                        SetBloom(bloomSet.GetCurrentValue());
                        break;
                    case 5:
                        SetSaturation(saturationSet.GetCurrentValue());
                        break;
                    case 6:
                        SetGamma(gammaSet.GetCurrentValue());
                        break;
                    default:
                        break;
                }
                CheckApplyButtonState(videoApplyButton, allVideoSettings);
            }
            else
            {
                switch (mySelection)
                {
                    case 7:
                        ApplyVideoSettings();
                        break;
                    case 8:
                        PauseMenu();
                        break;
                    default:
                        break;
                }
            }         
        }
        else if (selector == gameplayMenuSelector)
        {
            // Increase or decrease the gameplay setting value that is currently selected
            if (mySelection <= 5)
            {
                if (dir == Direction.Right) { allGameplaySettings[mySelection].Increase(); }
                else if (dir == Direction.Left) { allGameplaySettings[mySelection].Decrease(); }
                CheckApplyButtonState(gameplayApplyButton, allGameplaySettings);
            }
            else
            {
                switch (mySelection)
                {
                    case 6:
                        ApplyGameplaySettings();
                        break;
                    case 7:
                        PauseMenu();
                        break;
                    default:
                        break;
                }
            }           
        }
        RefreshValues();
    }

    // Check if certain setting category has been modified and Apply -button should be interactable
    void CheckApplyButtonState(Button button, List<Setting> targetSettings)
    {
        foreach (Setting set in targetSettings)
        {
            if (!set.IsSavedValue())
            {
                button.interactable = true;
                return;
            }
        }
        button.interactable = false;
    }

    // Animation of the pause menu. Menu pops from above or below depending of the vertical direction user has pressed
    public void PauseMenuAnimation(Direction dir)
    {
        lastDirection = dir;
        switch (dir)
        {
            case Direction.Up:
                if (pauseMenuOn == false)
                {
                    pauseMenu.transform.position = new Vector2(Screen.width / 2, Screen.height / 2 - Screen.height * 2);
                    Vector2 targetUp = new Vector2(Screen.width / 2, Screen.height / 2);                   
                    StartCoroutine(MoveUIToPos(pauseMenu, targetUp, 0.35f, true));
                    RetrieveValues();
                }
                else if (pauseMenuOn)
                {
                    Vector2 targetUp = new Vector2(Screen.width / 2, Screen.height / 2 + Screen.height * 2);
                    StartCoroutine(MoveUIToPos(pauseMenu, targetUp, 0.35f, false));
                }
                break;

            case Direction.Down:
                if (pauseMenuOn == false)
                {
                    pauseMenu.transform.position = new Vector2(Screen.width / 2, Screen.height / 2 + Screen.height * 2);
                    Vector2 targetDown = new Vector2(Screen.width / 2, Screen.height / 2);                  
                    StartCoroutine(MoveUIToPos(pauseMenu, targetDown, 0.35f, true));
                    RetrieveValues();
                }
                else if (pauseMenuOn)
                {
                    Vector2 targetDown = new Vector2(Screen.width / 2, Screen.height / 2 - Screen.height * 2);
                    StartCoroutine(MoveUIToPos(pauseMenu, targetDown, 0.35f, false));
                }
                break;
        }
    }

    // Move selector / navigator to any position. Also set the movement duration.
    IEnumerator MoveSelectorToPos(GameObject gb, Vector2 toPosition, float duration)
    {
        float counter = 0;
        Vector2 startPos = gb.transform.localPosition;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            gb.transform.localPosition = Vector2.Lerp(startPos, toPosition, counter / duration);
            yield return null;
        }
    }

    // Move UI element to any position and set the pauseMenu state on or off
    IEnumerator MoveUIToPos(GameObject gb, Vector2 toPosition, float duration, bool pauseMenuState)
    {
        pauseMenuOn = true;
        float counter = 0;

        Vector2 startPos = gb.transform.position;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            gb.transform.position = Vector2.Lerp(startPos, toPosition, counter / duration);
            yield return null;
        }

        pauseMenuOn = pauseMenuState;
    }
}
