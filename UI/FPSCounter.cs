﻿using UnityEngine;
using TMPro;

// Script for calculating current frame rate

public class FPSCounter : MonoBehaviour
{
    // Current frame rate
    private float current;

    // The average frame rate
    public int avgFrameRate;

    // Text which shows the average fps
    public TMP_Text display_Text;

    // Refresh interval of the text object
    private float refreshRate = 1f;
    
    // Timer for updating the fps information
    private float timer;

    // Show current FPS on the text object
    public void Update()
    {
        if (Time.unscaledTime > timer)
        {
            current = 0;
            current = (int)(1f / Time.unscaledDeltaTime);
            avgFrameRate = (int)current;
            display_Text.text = avgFrameRate.ToString() + " FPS";
            timer = Time.unscaledTime + refreshRate;
        }
    }
}
