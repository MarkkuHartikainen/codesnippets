﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

// This script handles the functions of the main menu

public class MainMenuCanvas : MonoBehaviour
{
    // List of different menu gameObjects aka. canvases
    public GameObject mainMenu;
    public GameObject emailLogInMenu;
    public GameObject successMenu;
    public GameObject errorMenu;

    // Inputfields in the email login section
    public TMP_InputField emailField;
    public TMP_InputField passwordField;

    // Selectors are used as navigators in the menu
    public GameObject mainMenuSelector;
    public GameObject emailMenuSelector;

    // Current selector / navigator depending of the active menu canvas
    private GameObject selector;

    // These gameObjects hold the email login and google login scripts
    public GameObject emailLogIn;
    public GameObject googleLogIn;

    // Current selected menu option / item
    private int mySelection = 0;

    void Start()
    {
        // Inform the control manager, that this script needs the user input information
        ControlManager.OnDirectionInput += ControlHandler_OnDirectionInput;

        // Reset text fields
        emailField.text = "";
        passwordField.text = "";
        SetEmailField();

        // Hide unused menus
        emailLogInMenu.SetActive(false);
        successMenu.SetActive(false);
        errorMenu.SetActive(false);

        // At first selector / navigator is in the mainMenu
        selector = mainMenuSelector;
    }

    // Handle D-pad and touch screen direction actions
    private void ControlHandler_OnDirectionInput(DirectionData data)
    {
        // If login succeeded or failed, use these functions no matter what input
        if (successMenu.activeSelf)
        {
            LoadGame();
            return;
        }
        else if(errorMenu.activeSelf)
        {
            MainMenu();
            return;
        }

        // What happens with each input
        switch (data.direction)
        {
            case Direction.Up:
                MoveSelector(-1);
                break;
            case Direction.Down:
                MoveSelector(1);
                break;
            case Direction.Tap:
                UseSelector();
                break;
            default:
                break;
        }
    }

    // Move selector gameObject to certain direction
    private void MoveSelector(int increment)
    {
        DeActivateInputFields();

        // Possible selector positions in the main menu
        if (mainMenu.activeSelf)
        {
            mySelection = Mathf.Clamp(mySelection + increment, 0, 2);

            switch (mySelection)
            {
                case 0:
                    selector.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(1100f, 120f);
                    selector.transform.localPosition = new Vector2(selector.transform.localPosition.x, 58f);
                    break;
                case 1:
                    selector.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(1100f, 120f);
                    selector.transform.localPosition = new Vector2(selector.transform.localPosition.x, -69f);
                    break;
                case 2:
                    selector.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(1100f, 80f);
                    selector.transform.localPosition = new Vector2(selector.transform.localPosition.x, -218f);
                    break;
                default:
                    break;
            }
        }
        // Possible selector positions in the email login menu
        else if (emailLogInMenu.activeSelf)
        {
            mySelection = Mathf.Clamp(mySelection + increment, 0, 3);

            switch (mySelection)
            {
                case 0:
                    selector.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(1100f, 160f);
                    selector.transform.localPosition = new Vector2(selector.transform.localPosition.x, 134f);
                    break;

                case 1:
                    selector.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(1100f, 160f);
                    selector.transform.localPosition = new Vector2(selector.transform.localPosition.x, -59f);
                    break;

                case 2:
                    selector.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(1100f, 80f);
                    selector.transform.localPosition = new Vector2(selector.transform.localPosition.x, -211f);
                    break;

                case 3:
                    selector.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(1100f, 80f);
                    selector.transform.localPosition = new Vector2(selector.transform.localPosition.x, -294f);
                    break;

                default:
                    break;
            }
        }

        DeActivateInputFields();
    }

    // Use selector (aka. choose menu item) according to what option / menu item is selected
    private void UseSelector()
    {
        // These options are available on the email login menu
        if (emailLogInMenu.activeSelf)
        {
            switch (mySelection)
            {
                case 0:
                    emailField.ActivateInputField();
                    break;

                case 1:
                    passwordField.ActivateInputField();
                    break;

                case 2:
                    EmailLogInButton();
                    break;

                case 3:
                    MainMenu();
                    break;

                default:
                    break;
            }
        }
        // These options are available on the main menu
        else if (mainMenu.activeSelf)
        {
            switch (mySelection)
            {
                case 0:
                    googleLogIn.GetComponent<FirebaseGoogleLogIn>().LogInWithGoogle();
                    break;
                case 1:
                    EmailLogInMenu();
                    break;
                case 2:
                    QuitButton();
                    break;
                default:
                    break;
            }
        }

    }

    // Set email field for writing
    public void SetEmailField()
    {
        emailField.text = emailField.text.Trim();

        if (emailField.text.Length == 0)
        {
            emailField.text = emailField.text.Insert(emailField.text.Length, "@metalo.fi");           
        }

        emailField.caretPosition = 0;
        DeActivateInputFields();
    }

    // Deactivate email login menu inputfields
    private void DeActivateInputFields()
    {
        emailField.DeactivateInputField();
        passwordField.DeactivateInputField();
    }

    // Email log in -button function
    public void EmailLogInButton()
    {
        emailLogIn.GetComponent<FirebaseEmailLogIn>().LogIn(emailField.text.Trim(), passwordField.text.Trim());
    }

    // Quit -button funtion
    public void QuitButton()
    {
        print("QuitGame");
        Application.Quit();
    }

    // Activate login succeeded -menu
    public void LoginSuccess()
    {
        mainMenu.SetActive(false);
        emailLogInMenu.SetActive(false);
        errorMenu.SetActive(false);
        successMenu.SetActive(true);
    }

    // Activate login failed -menu
    public void LoginFailed()
    {
        mainMenu.SetActive(false);
        emailLogInMenu.SetActive(false);
        successMenu.SetActive(false);
        errorMenu.SetActive(true);
    }

    // Load the game scene
    public void LoadGame()
    {
        mySelection = 0;
        SceneManager.LoadScene(1);
    }

    // Activate main menu -menu
    public void MainMenu()
    {
        successMenu.SetActive(false);
        errorMenu.SetActive(false);
        emailLogInMenu.SetActive(false);
        mainMenu.SetActive(true);

        selector = mainMenuSelector;
        mySelection = 0;
        MoveSelector(0);
    }

    // Activate email log in -menu
    public void EmailLogInMenu()
    {
        successMenu.SetActive(false);
        errorMenu.SetActive(false);
        mainMenu.SetActive(false);
        emailLogInMenu.SetActive(true);

        selector = emailMenuSelector;
        mySelection = 0;
        MoveSelector(0);
    }
}
