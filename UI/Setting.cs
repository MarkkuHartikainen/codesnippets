﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

// Class for different settings 
public class Setting
{
    // Name of this setting
    string myName;

    // Inputfield of this setting
    TMP_InputField mySettingBox;
    
    // Left arror Image of this setting 
    Image leftArrow;

    // Right arrow Image of this setting
    Image rightArrow;

    // Current value of this setting
    float currentValue;

    // Saved value of this setting
    float savedValue;

    // Minimum value of this setting
    float minValue;

    // Maximum value of this setting
    float maxValue;

    // Amount of increase or decrease per setting value modification
    float incrementAmount;

    // Category of this setting
    SettingsManager.SettingCategory myCategory;

    // Creating new setting and finding the arrow images
    public Setting(string name, TMP_InputField parent, float value, float min, float max, float increment, SettingsManager.SettingCategory category)
    {
        myName = name;
        mySettingBox = parent;
        leftArrow = parent.transform.Find("LeftArrow").GetComponentInChildren<Image>();
        rightArrow = parent.transform.Find("RightArrow").GetComponentInChildren<Image>();
        minValue = min;
        maxValue = max;
        incrementAmount = increment;
        myCategory = category;

        // If there is no already saved setting value, save and use the default value
        if (!PlayerPrefs.HasKey(myName))
        {
            PlayerPrefs.SetFloat(myName, value);
        }

        // Retrieve the saved value of this setting
        savedValue = PlayerPrefs.GetFloat(myName);        
        SetCurrentValue(savedValue);

        AddToList();
    }

    // Add this setting to correct list according to its category
    private void AddToList()
    {
        switch (myCategory)
        {
            case SettingsManager.SettingCategory.video:
                SettingsManager.allVideoSettings.Add(this);
                break;
            case SettingsManager.SettingCategory.gameplay:
                SettingsManager.allGameplaySettings.Add(this);
                break;
            default:
                break;
        }
        SettingsManager.allSettings.Add(this);
    }

    // Returns current value of this setting
    public float GetCurrentValue()
    {
        return currentValue;
    }

    // Returns saved value of this setting
    public float GetSavedValue()
    {
        return savedValue;
    }

    // Set current value of this setting and handle the arrow images
    public void SetCurrentValue(float value)
    {
        currentValue = value;
        ChangeAlpha(rightArrow, 1f);
        ChangeAlpha(leftArrow, 1f);
        if (currentValue == maxValue) { ChangeAlpha(rightArrow, 0.2f); return; }
        else if (currentValue == minValue) { ChangeAlpha(leftArrow, 0.2f); return; }
    }

    // Increase current value of this setting by increase amount. Also handle the arrow images.
    public void Increase()
    {
        currentValue = Mathf.Clamp(currentValue + incrementAmount, minValue, maxValue);

        if (currentValue == maxValue)
        {
            ChangeAlpha(rightArrow, 0.2f);
        }

        ChangeAlpha(leftArrow, 1f);
    }

    // Decrease current value of this setting by "increase" amount. Also handle the arrow images.
    public void Decrease()
    {
        currentValue = Mathf.Clamp(currentValue - incrementAmount, minValue, maxValue);

        if (currentValue == minValue)
        {
            ChangeAlpha(leftArrow, 0.2f);
        }

        ChangeAlpha(rightArrow, 1f);
    }

    // Change alpha color of the arrow image
    private void ChangeAlpha(Image target, float alpha)
    {
        Color temp = target.color;
        temp.a = alpha;
        target.color = temp;
    }

    // Save current value
    public void SaveValue()
    {
        savedValue = currentValue;
        PlayerPrefs.SetFloat(myName, currentValue);
    }

    // Return settings box Y-position (for selector / navigation movement)
    public float GetBoxY()
    {
        Vector2 pos = mySettingBox.transform.localPosition;
        return pos.y;
    }

    // Returns true if current value is saved value (used to determine whether apply button should be interactable)
    public bool IsSavedValue()
    {
        if (currentValue == savedValue)
        {
            return true;
        }
        return false;
    }
}
