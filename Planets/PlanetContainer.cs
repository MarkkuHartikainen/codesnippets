﻿using System.Collections;
using UnityEngine;

// Container of a planet object

public class PlanetContainer : MonoBehaviour
{
    // Planet speed when moving to new orbit radius distance
    public float speed = 1f;

    // Planet camera of this planet
    public GameObject planetCam;

    // Orbit radius of this planet
    private float radius;

    // Upper or lower level parent object that rotates this planet
    private Transform myRotator;

    // At first, disable the planet camera
    void Awake()
    {
        planetCam.SetActive(false);
    }

    void Start()
    {
        // Find parent
        myRotator = transform.parent;
    }

    // Return planet's camera
    public GameObject GetPlanetCam()
    {
        return planetCam;
    }

    // Update planet position according to the new radius
    public void UpdateRadius(float newRadius)
    {
        StopAllCoroutines();

        Vector3 direction = myRotator.position - transform.position;
        direction.Normalize();

        bool increase = radius < newRadius;
        radius = newRadius;

        StartCoroutine(UpdateRad(direction, newRadius, increase));
    }

    // Increase or decrease planet orbit radius
    IEnumerator UpdateRad(Vector3 direction, float radius, bool isIncreasing)
    {
        if (isIncreasing)
        {
            direction *= -1;
            while (Vector3.Distance(myRotator.position, transform.position) < radius)
            {
                transform.position = transform.position + direction * speed * Time.deltaTime;
                yield return null;
            }
        }
        else
        {
            while (Vector3.Distance(myRotator.position, transform.position) > radius)
            {
                transform.position = transform.position + direction * speed * Time.deltaTime;
                yield return null;
            }
        }
    }     
}
