﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Solar system handles all the planet positions both on the upper and the lower level

public class SolarSystem : MonoBehaviour
{
    // Solar system's center position, which is the same as the current star pattern's center
    private Vector3 myCenterPos = new Vector3(0f, 0f, 0f);

    // Speed with which solar system moves to current center position
    public float movementSpeed = 1f;

    // Orbit radius of the planets
    public float planetRadius = 25f;

    // Vertical distance of the planets from the stars
    public float planetYDistance = 15f;
    
    // Parent object for upper level planets
    private GameObject upperLevel;

    // Parent object for lower level planets
    private GameObject lowerLevel;

    // List of spawnable planet prefabs 
    public List<GameObject> planetPrefabs = new List<GameObject>();

    // List of spawned planet objects
    private List<GameObject> listOfPlanets = new List<GameObject>();

    // Amount of planets on upper level
    private int upperLevelCount = 0;

    // Amount of planets on lower level
    private int lowerLevelCount = 0;

    // Spawn planets on upper and lower levels
    void Start()
    {
        // Find parent objects for upper and lower level planets
        upperLevel = transform.GetChild(0).gameObject;
        lowerLevel = transform.GetChild(1).gameObject;

        // Put parents to their default position
        upperLevel.transform.position = new Vector3(transform.position.x, transform.position.y + 15f, transform.position.z);
        lowerLevel.transform.position = new Vector3(transform.position.x, transform.position.y - 15f, transform.position.z);

        // Distribute upper and lower level planets equally
        upperLevelCount = Mathf.RoundToInt(planetPrefabs.Count / 2f);
        lowerLevelCount = planetPrefabs.Count - upperLevelCount;

        // Spawns upper and lower level planets on orbit and adds them on the list of planets
        for (int i = 0; i < planetPrefabs.Count; i++)
        {
            if (i < upperLevelCount)
            {
                float angle = i * Mathf.PI * 2 / upperLevelCount;
                Vector3 planetPos = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * planetRadius;
                planetPos += upperLevel.transform.position;

                var newPlanetContainer = Instantiate(planetPrefabs[i], planetPos, transform.rotation);
                newPlanetContainer.transform.parent = upperLevel.transform;
                listOfPlanets.Add(newPlanetContainer);
            }
            else
            {
                float angle = i * Mathf.PI * 2 / lowerLevelCount;
                Vector3 planetPos = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * planetRadius;
                planetPos += lowerLevel.transform.position;

                var newPlanetContainer = Instantiate(planetPrefabs[i], planetPos, transform.rotation);
                newPlanetContainer.transform.parent = lowerLevel.transform;
                listOfPlanets.Add(newPlanetContainer);
            }
        }
    }

    // Update planet positions based on solar system's new center position, current star pattern and new radius
    public void UpdatePlanetPositions(Vector3 newCenter, float maxY, float minY)
    {
        myCenterPos = newCenter;
        
        // New radius is main camera's distance to star center + planetRadius
        float radius = Vector3.Distance(myCenterPos, GameManager.cameraPosition) + planetRadius;

        foreach (GameObject planet in listOfPlanets)
        {
            planet.GetComponent<PlanetContainer>().UpdateRadius(radius);
        }

        upperLevel.GetComponent<Movement>().MoveToPosition(new Vector3(myCenterPos.x, maxY + planetYDistance, transform.position.z));
        lowerLevel.GetComponent<Movement>().MoveToPosition(new Vector3(myCenterPos.x, minY - planetYDistance, transform.position.z));

        StopAllCoroutines();
        StartCoroutine(MoveToPosition(newCenter));
    }

    // Activate random planet camera or just disable them
    public void SetPlanetCam(bool value)
    {
        foreach (GameObject planet in listOfPlanets)
        {
            planet.GetComponent<PlanetContainer>().GetPlanetCam().SetActive(false);
        }

        if (value == true)
        {
            listOfPlanets[Random.Range(0, listOfPlanets.Count)].GetComponent<PlanetContainer>().GetPlanetCam().SetActive(true);
        }
    }

    // Move solar system to target position
    IEnumerator MoveToPosition(Vector3 target)
    {
        while (Vector3.Distance(target, transform.position) >= 0.1f)
        {
            float step = movementSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target, step);
            yield return null;
        }
    }
}

